﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthAndJetPack : MonoBehaviour
{

    public Image health;
    public Image jetPack;
    public Transform damage;

    private float healthVal = 100;
    private float jetPackVal = 100;
    void Update()
    {
        if(GameObject.FindGameObjectWithTag("Player").GetComponent<JetpackCharge>().GetJetpackCharge() != jetPackVal)
        {
            jetPack.fillAmount = GameObject.FindGameObjectWithTag("Player").GetComponent<JetpackCharge>().GetJetpackCharge() / 100;
            jetPackVal = GameObject.FindGameObjectWithTag("Player").GetComponent<JetpackCharge>().GetJetpackCharge();
        }
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().getHealth() != healthVal)
        {
            health.fillAmount = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().getHealth() / 100;
            healthVal = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().getHealth();
            damage.gameObject.SetActive(true);
            Invoke("DamageOff", 0.5f);
        }
            
    }

    void DamageOff()
    {
        damage.gameObject.SetActive(false);
    }
}

