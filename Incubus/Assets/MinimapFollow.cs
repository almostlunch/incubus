﻿using UnityEngine;
using System.Collections;

public class MinimapFollow : MonoBehaviour {

    private GameObject player;
	// Use this for initialization
	void Start () {
	
        
	}
	
	// Update is called once per frame
	void Update () {
	
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");

        }

        Vector3 temp = gameObject.transform.position;
        temp.x = player.transform.position.x;
        temp.z = player.transform.position.z;
        gameObject.transform.position = temp;
	}
}
