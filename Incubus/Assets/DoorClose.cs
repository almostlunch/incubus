﻿using UnityEngine;
using System.Collections;

public class DoorClose : MonoBehaviour {

    private bool doneSlam = false;
    float timer;

    void Update()
    {
        if(doneSlam && GameObject.FindGameObjectsWithTag("Damageable").Length == 0 && timer <= 0)
        {
            Debug.Log("here");
            transform.FindChild("Trigger").GetComponent<OpenDoor>().doorOpened = false;
            doneSlam = false;
        }

        if(timer > 0)
        {
            timer -= Time.deltaTime;
        }
    }

	void SlamDoor()
    {
        transform.GetComponent<Animator>().Play("DoorSlam");
        doneSlam = true;
        timer = 10;
    }

    void SearchRoom()
    {
        Invoke("Wait", 1);
    }

    void Wait()
    {
        GameObject.FindGameObjectWithTag("ScriptManager").GetComponent<RoomFinder>().SearchRoom();
    }
}
