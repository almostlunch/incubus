﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScreen : MonoBehaviour {

	public void AlmostLunchLink()
    {
        Application.OpenURL("www.almostlunchgames.wordpress.com");
    }

    public void ItchLink()
    {
        Application.OpenURL("www.almostlunch.itch.io");
    }
}
