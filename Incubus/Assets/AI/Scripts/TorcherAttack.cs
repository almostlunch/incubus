﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TorcherAttack : MonoBehaviour {

    private Transform p_Transform;
    private Navfollow myNavFollow;
    private UnityEngine.AI.NavMeshAgent myNavAgent;
    private Vector3 chargeTarget;

    public GameObject particles;
    private bool partOn = false;
    private int health;

    public GameObject expRadius;

    private Animator myAnimator;

    private int suicidal;

    //Attack states
    private bool shambling = true;
    private bool chasing = false;
    private bool charging = false;
    private bool exploding = false;

    private bool damageable = false;

    //tiles
    List<Transform> tiles = new List<Transform>();


    void Start()
    {
        myNavFollow = transform.GetComponent<Navfollow>();
        myNavAgent = transform.GetComponent<UnityEngine.AI.NavMeshAgent>();
        myAnimator = transform.GetComponent<Animator>();
        p_Transform = GameObject.FindGameObjectWithTag("Player").transform;

        health = 30;
        suicidal = Random.Range(0, health);

        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Tile"))
        {
            tiles.Add(g.transform);
        }

        Shamble();
        myNavFollow.chasing = false;
    }

    void Update()
    {
        if(transform.GetComponent<Damageable>().health != health && !partOn)
        {
            particles.SetActive(true);
            transform.GetComponent<AudioSource>().Play();
            partOn = true;
            newAction();
        }

        if(transform.GetComponent<Damageable>().GetHealth() < suicidal)
        {
            States(4);
        }
            //attack states

        if (shambling)
        {
            Invoke("Shamble", 3);
            shambling = false;
        }

        if(chasing)
        {
            myNavFollow.chasing = true;
            myAnimator.Play("ChildRun");
            myNavAgent.speed = 5;
            int i = Random.Range(4, 10);
            Invoke("newAction", i);
            if (Random.Range(0, 10) == 2)
            {
                BlindRage();
            }
            chasing = false;

        }

        if(Vector3.Distance(transform.position, p_Transform.position) <= 3.5f && damageable)
        {
            DoDamage();

        }

        if (charging)
        {
            myNavFollow.chasing = false;
            myAnimator.Play("ChildAttack");
            myNavAgent.speed = 10;
            myNavAgent.SetDestination(p_Transform.position);
            transform.LookAt(p_Transform.position);
            damageable = true;
            if(Random.Range(0, 10) == 2)
            {
                BlindRage();
            }
            Invoke("newAction", 4);
            charging = false;
            
        }

        if(exploding)
        {
            myNavFollow.chasing = true;
            Invoke("Explode", 3);
            myAnimator.Play("ChildRun");
            myNavAgent.speed = 10;
            exploding = false;
        }


    }

    void DoDamage()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().TakeDamage(10);
        damageable = false;
    }

    void Shamble()
    {
        damageable = false;
        myNavFollow.chasing = false;
        myAnimator.Play("ChildShamble");
        myNavAgent.speed = 1;
        int i = Random.Range(0, tiles.Count);
        myNavAgent.SetDestination(tiles[i].position);
        transform.LookAt(tiles[i].position); shambling = true;
    }

    void BlindRage()
    {

        myNavFollow.chasing = false;
        int i = Random.Range(0, tiles.Count);
        myNavAgent.SetDestination(tiles[i].position);
        transform.LookAt(tiles[i].position);
    }

    void newAction()
    {
        CancelInvoke();
        int i = Random.Range(2, 4);
        shambling = false;
        if(i == 2)
        {
            States(2);
        }

        if(i == 3)
        {
            States(3);
        }
    }


    void OnDestroy()
    {
        expRadius.transform.position = transform.position;
        expRadius.SetActive(true);
    }
    void Explode()
    {
        CancelInvoke();
        Destroy(transform.gameObject);
    }

    void States(int state)
    {
        if (state == 1)
        {
            shambling = true;
            chasing = false;
            charging = false;
            exploding = false;
        }

        if (state == 2)
        {
            shambling = false;
            chasing = true;
            charging = false;
            exploding = false;
        }

        if (state == 3)
        {
            shambling = false;
            chasing = false;
            charging = true;
            exploding = false;
        }

        if (state == 4)
        {
            shambling = true;
            chasing = false;
            charging = false;
            exploding = true;
        }

    }
}
