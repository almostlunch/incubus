﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public float force = 1.0f;
    private Rigidbody m_rigidbody;
    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_rigidbody.velocity = (transform.forward * force);
        StartCoroutine("Destroy");

    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(10f);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().TakeDamage(5);
        }

        if(other.tag == "Wall")
        {
            Destroy(gameObject);
        }

    }

}
