﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CyclopsSpawning : MonoBehaviour {

    public GameObject Cyclops;
    private int counter = 0;

    public void spawnEnemy()
    {
        

        Instantiate(Cyclops, transform.position, transform.rotation);


        counter++;

        if (counter > Random.Range(2, 6))
        {
            Destroy(transform.parent.gameObject);
        }

        
    }


}