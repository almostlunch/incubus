﻿using UnityEngine;
using System.Collections;

public class FireLaser : MonoBehaviour {

    private LineRenderer laserBeam;
    private float counter;
    private float dist;

    private Transform startPos;
    public Transform endPos;

    public float laserSpeed = 6f;

    void Start()

    {
        startPos = transform.FindChild("shoot");
        endPos = transform.FindChild("shootEnd");
        laserBeam = GetComponent<LineRenderer>();
        laserBeam.SetPosition(0, startPos.position);
        laserBeam.SetWidth(0.2f, 0.2f);

        dist = Vector3.Distance(startPos.position, endPos.position);
    }

    void Update()

    {
        
        laserBeam.SetPosition(0, startPos.position);
        if (counter < dist)
        {
            counter += 0.1f / laserSpeed;

            float x = Mathf.Lerp(0, dist, counter);

            Vector3 pointA = startPos.position;
            Vector3 pointB = endPos.position;

            Vector3 pointAlongLine = x * Vector3.Normalize(pointB - pointA) + pointA;

            laserBeam.SetPosition(1, pointAlongLine);
        }
    }
}
