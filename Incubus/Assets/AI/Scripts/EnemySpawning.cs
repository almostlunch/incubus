﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawning : MonoBehaviour {
    private int numberOfEnemies;
    private int EnemyIndex;
    private int EnemiesSpawned = 0;

    List<int> EnemyIndices;

    public GameObject Imp;
    public GameObject Cyclops;
    public GameObject Torcher;

    List<GameObject> Imps;
    List<GameObject> Cyclopses;
    List<GameObject> Torchers;

    bool spawnedOne = false;

    private int counter = 0;


    // Use this for initialization
    void Start()
    {

    }

    public void spawnEnemy()
    {
        if(counter == 0)
            Instantiate(Imp, transform.position, transform.rotation);
        else if( counter == 1)
            Instantiate(Cyclops, transform.position, transform.rotation);
        else if(counter == 2)
            Instantiate(Torcher, transform.position, transform.rotation);
        else if (counter == 3)
            Instantiate(Torcher, transform.position, transform.rotation);
        else if (counter == 4)
            Instantiate(Imp, transform.position, transform.rotation);
        else
            Instantiate(Cyclops, transform.position, transform.rotation);




        counter++;

        if (counter > Random.Range(2, 6))
        {
            Destroy(transform.parent.gameObject);
        }

    }


}