﻿using UnityEngine;
using System.Collections;

public class LaserDamage : MonoBehaviour {
    float timer = 0.5f;

    void Update()
    {
        if(timer > 0)
            timer -= Time.deltaTime; 
    }

	void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player" && timer <= 0)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().TakeDamage(5);
            timer = 1.1f;
        }
    }
}
