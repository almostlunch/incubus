﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DemonLists : MonoBehaviour {

    public GameObject Imp;
    public GameObject Cyclops;
    public GameObject Torcher;
    public GameObject r_Imp;
    public GameObject r_Cyclops;
    public GameObject r_Torcher;

    public List<GameObject> Imps;
    public List<GameObject> Cyclopses;
    public List<GameObject> Torchers;
    public List<GameObject> r_Imps;
    public List<GameObject> r_Cyclopses;
    public List<GameObject> r_Torchers;


    private int impPool = 24;
    private int cyclopsPool = 24;
    private int TorcherPool = 24;

    void Start()
    {
        Imps = new List<GameObject>();
        Cyclopses = new List<GameObject>();
        Torchers = new List<GameObject>();
        r_Imps = new List<GameObject>();
        r_Cyclopses = new List<GameObject>();
        r_Torchers = new List<GameObject>();

        for (int i = 0; i < impPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(Imp);
            obj.SetActive(false);
            Imps.Add(obj);
            GameObject r_obj = (GameObject)Instantiate(r_Imp);
            r_obj.SetActive(false);
            r_Imps.Add(r_obj);

        }

        for (int i = 0; i < TorcherPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(Torcher);
            obj.SetActive(false);
            Torchers.Add(obj);
            GameObject r_obj = (GameObject)Instantiate(r_Torcher);
            r_obj.SetActive(false);
            r_Torchers.Add(r_obj);

        }

        for (int i = 0; i < cyclopsPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(Cyclops);
            obj.SetActive(false);
            Cyclopses.Add(obj);
            GameObject r_obj = (GameObject)Instantiate(r_Cyclops);
            r_obj.SetActive(false);
            r_Cyclopses.Add(r_obj);

        }
    }

}
