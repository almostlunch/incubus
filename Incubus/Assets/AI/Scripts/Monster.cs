﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour {

    protected int health;    

    public void takeDamage(int damage)
    {
        health -= damage;
        
        if(health <= 0)
        {
            Debug.Log("Got here!!");
            this.gameObject.SetActive(false);
        }
    }
}
