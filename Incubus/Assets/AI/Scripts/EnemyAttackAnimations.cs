﻿using UnityEngine;
using System.Collections;

public class EnemyAttackAnimations : MonoBehaviour {


   
    private Transform myTransform;
    private Transform p_Transform;
    private UnityEngine.AI.NavMeshAgent myAgent;
    private Navfollow myFollow;
    private Animator myAnimator;
    private float distanceToAttack = 3;
    private float distanceToShoot = 15;

    private float pAttackCD;
    private float sAttackCD;

    private bool pAttackReady;
    private bool sAttackReady;

    private bool anHero;

    private bool attack;

    private bool backOff;

    private bool shootingAim;
	// Use this for initialization
	void Start () {

        myTransform = transform;

        p_Transform = GameObject.FindGameObjectWithTag("Player").transform;

        myAgent = transform.parent.GetComponent<UnityEngine.AI.NavMeshAgent>();
        myFollow = transform.parent.GetComponent<Navfollow>();

        myAnimator = transform.GetComponent<Animator>();

        if(Random.Range(1, 4) != 2)
        {
            anHero = true;
        }
        else
        {
            anHero = false;
        }
	
	}
	
	// Update is called once per frame
	void Update () {

        if(pAttackCD <=0)
        {
            pAttackReady = true;
        }
        else
        {
            pAttackReady = false;
            pAttackCD -= Time.deltaTime;
        }

        if(sAttackCD <= 0)
        {
            sAttackReady = true;
        }
        else
        {
            sAttackReady = false;
            sAttackCD -= Time.deltaTime;
        }

        if(anHero)
        {
            if (Vector3.Distance(myTransform.position, p_Transform.position) < distanceToAttack && pAttackReady)
            {
                Attack();
            }
        }
        else
        {
            if (Vector3.Distance(myTransform.position, p_Transform.position) < distanceToShoot && sAttackReady)
            {
                Shoot();
            }
        }

        if(shootingAim)
        {
            transform.parent.rotation = Quaternion.Slerp(transform.parent.rotation, Quaternion.LookRotation(new Vector3(p_Transform.position.x, transform.parent.position.y, p_Transform.position.z) - transform.parent.position), (3 * Time.deltaTime));
        }
    }

    void Attack()
    {
        myAgent.stoppingDistance = 0;
        pAttackCD = 2;
        myAnimator.Play("CyclopsAttack");
       
    }

    public void MeleeDamage()
    {
        if (Vector3.Distance(myTransform.position, p_Transform.position) <= 3.5f)
        {
            pAttackCD = 5;
            p_Transform.GetComponent<PlayerHealth>().TakeDamage(30);
        }
    }

    void Shoot()
    {
        myAgent.stoppingDistance = 15;
        sAttackCD = 10;
        myFollow.chasing = false;
        myAnimator.Play("CyclopsShoot");
        myAgent.SetDestination(transform.position);
    }
    

    void Chase()
    {
        myFollow.chasing = true;
        myAnimator.Play("Run");
        myAgent.stoppingDistance = 1;
    }

    void LaserEnabled()
    {
        transform.FindChild("Pelvis").FindChild("Spine").FindChild("Head").FindChild("Laser").gameObject.SetActive(true);
        shootingAim = true;

    }

    void LaserDisabled()
    {
        transform.FindChild("Pelvis").FindChild("Spine").FindChild("Head").FindChild("Laser").gameObject.SetActive(false);
        shootingAim = false;
        Chase();
    }
}
