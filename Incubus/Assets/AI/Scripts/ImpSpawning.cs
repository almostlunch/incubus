﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ImpSpawning : MonoBehaviour {

    public GameObject Imp;
    private int counter = 0;
    

    public void spawnEnemy()
    {
        
        Instantiate(Imp, transform.position, transform.rotation);


        counter++;

        if (counter > Random.Range(2, 6))
        {
            Destroy(transform.parent.gameObject);
        }

        
    }


}