﻿using UnityEngine;
using System.Collections;

public class HatchSound : MonoBehaviour {


    public AudioClip thud;
    public AudioClip clang;
    private bool playedThud = false;
    private bool playedClang = false;


    void OnCollisionEnter(Collision c)
    {
        if(!gameObject.GetComponent<Rigidbody>().isKinematic)
        {
            if (c.gameObject.tag == "Wall")
            {
                if (!playedThud)
                {
                    gameObject.GetComponent<AudioSource>().clip = thud;
                    gameObject.GetComponent<AudioSource>().Play();
                    //playedThud = true;
                }

            }
            else if(c.gameObject.tag == "Rail" || c.gameObject.tag == "Pepe")
            {
                if (!playedClang)
                {
                    gameObject.GetComponent<AudioSource>().clip = clang;
                    gameObject.GetComponent<AudioSource>().Play();
                    //playedClang = true;
                }

            }
        }
        
    }
}
