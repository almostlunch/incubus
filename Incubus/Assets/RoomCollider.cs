﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class RoomCollider : MonoBehaviour {

    //portals 
    public Transform portal1;
    public Transform portal2;
    public Transform portal3;
    public Transform portal4;

    List<GameObject> inactive = new List<GameObject>();
    private string playerRoom;
    private bool initialRoom = false;
    private bool foundDoors = false;

    private List<GameObject> doors = new List<GameObject>();
    private List<string> roomsExplored = new List<string>();

    private Collider currentCollider;
    private bool clearLists = false;
    private bool inBoundingBox = false;
    private bool enemiesSpawning = false;
    private bool enemies = false;

    private string sceneName;

    private List<Transform> spawnTiles = new List<Transform>();

    void Start()
    {
        sceneName = SceneManager.GetActiveScene().name;
    }
    void Update()
    {

        if (sceneName != SceneManager.GetActiveScene().name)
        {
            Debug.Log("SceneChanged");
            inactive = new List<GameObject>();
            doors = new List<GameObject>();
            roomsExplored = new List<string>();
            spawnTiles = new List<Transform>();

            initialRoom = false;
            foundDoors = false;
            clearLists = false;
            inBoundingBox = false;
            enemiesSpawning = false;
            enemies = false;

            sceneName = SceneManager.GetActiveScene().name;
        }

        if (GameObject.FindGameObjectsWithTag("Damageable").Length == 0 && enemies && GameObject.FindGameObjectsWithTag("SpawnPortal").Length == 0)
        {
            enemiesSpawning = false;
            enemies = false;
        }

        if (!enemiesSpawning)
        {
            foreach (GameObject g in inactive)
            {
                if(g != null)
                    g.SetActive(true);
            }
            inactive = new List<GameObject>();

            foreach (GameObject g in GameObject.FindGameObjectsWithTag("Door"))
            {
                g.GetComponent<Animator>().Play("DoorOpen");

            }
        }

        

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "boundingBox")
            inBoundingBox = true;
    }
    void OnTriggerExit(Collider other)
    {
        if (other.name == "boundingBox")
            inBoundingBox = false;
    }
    void OnTriggerStay(Collider other)
    {
        if(!inBoundingBox)
        {
            if (other.tag == "Wall" && other.transform.parent.name != "Door 1(Clone)")
            {
                Debug.Log(other.transform.parent.name);
                if (!initialRoom)
                {
                    playerRoom = other.transform.parent.name;
                    roomsExplored.Add(playerRoom);
                    initialRoom = true;
                }

                if (other != currentCollider)
                {
                    currentCollider = other;
                }

                if (playerRoom != other.transform.parent.name)
                {
                    playerRoom = other.transform.parent.name;
                    if (!roomsExplored.Contains(playerRoom))
                    {
                        roomsExplored.Add(playerRoom);
                        Invoke("Unload", 0.1f);
                    }
                }
            }
        }
       
        
    }

    void Unload()
    {
        
            spawnTiles = new List<Transform>();
            for (int i = 0; i < GameObject.FindGameObjectWithTag("GameWorldManager").transform.childCount; i++)
            {
                if (GameObject.FindGameObjectWithTag("GameWorldManager").transform.GetChild(i).name != currentCollider.transform.parent.name)
                {
                }
                else
                {
                    if (Random.Range(0, 11) < 2)
                    {
                        spawnTiles.Add(GameObject.FindGameObjectWithTag("GameWorldManager").transform.GetChild(i).FindChild("Tile"));
                    }
                }
            }

            if (spawnTiles.Count != 0)
            {
                enemiesSpawning = true;
                for (int i = 0; i < GameObject.FindGameObjectWithTag("GameWorldManager").transform.childCount; i++)
                {
                    if (GameObject.FindGameObjectWithTag("GameWorldManager").transform.GetChild(i).name != currentCollider.transform.parent.name)
                    {
                        inactive.Add(GameObject.FindGameObjectWithTag("GameWorldManager").transform.GetChild(i).gameObject);
                        GameObject.FindGameObjectWithTag("GameWorldManager").transform.GetChild(i).gameObject.SetActive(false);
                    }
                }
            }




            if (enemiesSpawning)
            {
                foreach (GameObject g in GameObject.FindGameObjectsWithTag("Door"))
                {
                    g.GetComponent<Animator>().Play("DoorClose");
                }
                Invoke("Spawn", 3);
            }
        
        
    }


    void Spawn()
    {
        foreach(Transform t in spawnTiles)
        {
            if (t.parent.tag == "Type1")
                StartCoroutine(Spawn1(t));
            if (t.parent.tag == "Type2")
                StartCoroutine(Spawn2(t));
            if (t.parent.tag == "Type3")
                StartCoroutine(Spawn3(t));
            if (t.parent.tag == "Type4")
                StartCoroutine(Spawn4(t));

            Debug.Log(t.position);
        }
        Invoke("EnemiesDone", 10);
        
        
        
    }

    void EnemiesDone()
    {
        enemies = true;
    }

    IEnumerator Spawn1(Transform t)
    {
        yield return new WaitForSeconds(Random.Range(1, 10));
        if (t != null)
            Instantiate(portal1, new Vector3(t.position.x, portal1.position.y, t.position.z), portal1.rotation);
    }

    IEnumerator Spawn2(Transform t)
    {
        yield return new WaitForSeconds(Random.Range(1, 10));
        if (t != null)
            Instantiate(portal2, new Vector3(t.position.x, portal2.position.y, t.position.z), portal2.rotation);
    }

    IEnumerator Spawn3(Transform t)
    {
        yield return new WaitForSeconds(Random.Range(1, 10));
        if (t != null)
            Instantiate(portal3, new Vector3(t.position.x, portal3.position.y, t.position.z), portal3.rotation);
    }

    IEnumerator Spawn4(Transform t)
    {
        yield return new WaitForSeconds(Random.Range(1, 10));
        if(t != null)
            Instantiate(portal4, new Vector3(t.position.x, portal4.position.y, t.position.z), portal4.rotation);
    }

}
