﻿using UnityEngine;
using System.Collections;

public class Explodsion : MonoBehaviour {


    
    void Awake()
    {
        Invoke("Deactivate", 0.5f); 
    }

	void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().TakeDamage(30);

        }
        if(other.tag == "Damageable")
        {
            other.transform.GetComponent<Damageable>().TakeDamage(5);
        }
    }

    void Deactivate()
    {
        Destroy(gameObject);
    }
}
