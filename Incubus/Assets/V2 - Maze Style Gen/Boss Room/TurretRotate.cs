﻿using UnityEngine;
using System.Collections;

public class TurretRotate : MonoBehaviour {

    
    private Transform target;
    public float rotSpeed;

    private Quaternion lookRot;
    private Vector3 direction;
    
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }
	
	// Update is called once per frame
	void Update () {

        direction = (target.position - transform.position).normalized;

        lookRot = Quaternion.LookRotation(direction);

        transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime * rotSpeed);


	}
}
