﻿using UnityEngine;
using System;

[Serializable]
public class RoomSettings {
    
    //public Material floorMat, wallMat, floorMat2, wallMat2;
    public Material floorMat, wallMat, doorMat;
    public String roomType, wallType;
}
