﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //Variables
    public GameWorld levelPrefab;
    private GameWorld levelInstance;

	// Use this for initialization
	void Start () {
        Initialize();
	}

    // Update is called once per frame
    void Update()
    {

        if (GameObject.FindGameObjectWithTag("Portal") != null)
        {
            if (GameObject.FindGameObjectWithTag("Portal").GetComponent<EnterPortal>().entered == true)
            {
                {
                    GameObject.FindGameObjectWithTag("Portal").GetComponent<EnterPortal>().entered = false;
                    Restart();
                }
            }
        }
    }

    private void Initialize()
    {
        levelInstance = Instantiate(levelPrefab);
        StartCoroutine(levelInstance.Generate());
    }

    private void Restart()
    {
        StopAllCoroutines();
        //Destroy(levelInstance.gameObject);
        //Initialize();
        //Scene scene = SceneManager.GetActiveScene();
        GameObject.Find("DevModeStuff").GetComponent<DevModeStuff>().SetDevMode(true);
        SceneManager.LoadScene("ThankYouScene");
        //if (SceneManager.GetActiveScene().name == "Level1")
        //{
        //    SceneManager.LoadScene("Boss Room");
        //    //SceneManager.LoadScene("level2");
        //} else if (SceneManager.GetActiveScene().name == "Level2")
        //{
        //    SceneManager.LoadScene("Boss Room");
        //}
    }
}
