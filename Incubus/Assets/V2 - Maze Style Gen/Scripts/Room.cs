﻿using UnityEngine;
using System.Collections.Generic; // List

//scriptable so that unitry will keep references if a recompile happens during play mode
public class Room : ScriptableObject {

    public int settingsIndx;

    public RoomSettings settings;

    private List<LevelCell> cells = new List<LevelCell>();

    public void Add (LevelCell cell)
    {
        cell.room = this;
        cells.Add(cell);
    }

    //used to prevent open passageways connecting rooms, remove one room when it is assimilated by the other 
    public void Assimilate (Room room)
    {
        for (int i = 0; i < room.cells.Count; i++)
        {
            Add(room.cells[i]);
        }
    }

    //used to unload rooms the player is not in
    public void Hide()
    {
        for (int i = 0; i < cells.Count; i++)
        {
            cells[i].Hide();
        }
    }
    public void Show()
    {
        for (int i = 0; i < cells.Count; i++)
        {
            cells[i].Show();
        }
    }
}
