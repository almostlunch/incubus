﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; //used when creating list of cells to prevent gen from stopping
using UnityEngine.SceneManagement;

public class GameWorld : MonoBehaviour
{
    //VARIABLES:
    //public int sizeX, sizeZ; <- replaced with IntVector2
    public float cellGenerationDelay; //used to add a delay during generation to vusualise level generation
    public IntVector2 size; //used to set size of the dungeon (x, z)

    //prefabs etc,.
    public LevelCell cellPrefab; //cube with quad floor
    public LevelCell[,] cells; //array of cells
    public Passage passagePrefab; // empty cell
    public Wall[] wallPrefabs; //cell with wall on north 

    //spawn room
    public LevelCell spawnCellPrefab;
    bool firstCell = true;
    public bool canSpawn = false;

    //door system
    public Door doorPrefab; //door prefab attatched in editor
    [Range(0f, 1f)]
    public float doorProb; //door probibility


    //coloured room system
    public RoomSettings[] roomSettings;

    //room settings
    public GameObject glossyTile;
    public GameObject[] infectionPrefabs;
    public GameObject[] medkitCabinates;
    public GameObject portalPrefab;
    public GameObject pipesPrefab;

    //minimap
    public GameObject[] MiniMapTypes;

    //spawn info
    public int medCount = 0;
    public int mbC = 0;
    public int mC = 0;
    public int sC = 0;
    public int dnC = 0;

    public LevelCell GetCell(IntVector2 coordinates)
    {
        return cells[coordinates.x, coordinates.z];
    }

    public IEnumerator Generate()
    {
        WaitForSeconds genDelay = new WaitForSeconds(cellGenerationDelay);
        cells = new LevelCell[size.x, size.z];
        //list of active cells to prevent cell gen from halting upon bumping into each other
        List<LevelCell> activeCells = new List<LevelCell>();
        DoFirstGenStep(activeCells);
        while (activeCells.Count > 0)
        {
            yield return genDelay;
            DoNextGenStep(activeCells);
        }

        //spawn player
        canSpawn = true;

        //destoys asteroids after load:
        GameObject[] asts = GameObject.FindGameObjectsWithTag("Asteroids");
        foreach (GameObject ast in asts)
        {
            Destroy(ast);
        }

        //gen minimap
        GenMiniMap();

        //add objects to centre of rooms
        ApplyReflectiveSurfaces();
        ApplyInfection();
        //addPipes();
        Invoke("FindRooms", 1);

        //unload all rooms after gen
        /*for (int i = 0; i < rooms.Count; i++)
        {
            rooms[i].Hide();
        }*/
    }

    private void FindRooms()
    {
        List<GameObject> finders = new List<GameObject>();
        UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
        List<GameObject> toDelete = new List<GameObject>();
        foreach(GameObject g in GameObject.FindGameObjectsWithTag("RoomFinder"))
        {
            finders.Add(g);
        }

        foreach(GameObject g in finders)
        {
            g.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
        }

        for(int i = 0; i < finders.Count-1; i++)
        {
            foreach (GameObject g in finders)
            { 
                if (i < finders.IndexOf(g))
                {
                    path = new UnityEngine.AI.NavMeshPath();
                    finders[i].GetComponent<UnityEngine.AI.NavMeshAgent>().CalculatePath(g.transform.position, path);
                    if (path.status == UnityEngine.AI.NavMeshPathStatus.PathComplete)
                    {
                        toDelete.Add(g);
                    }
                }
            } 
        }

        foreach(GameObject g in toDelete)
        {
            finders.Remove(g);
            Destroy(g);

        }

        int t = 0;
        foreach(GameObject g in finders)
        {
            g.name = "Room " + t.ToString(); 
            g.GetComponent<RoomFinder>().SearchRoom();
            t++;
        }
        

    }

    private void ApplyReflectiveSurfaces()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("WallWhite");
        foreach (GameObject go in gos)
        {
            if (Random.Range(0.0f, 1.0f) > 0.95) {
                float choose = Random.Range(0.0f, 1.0f);
                if (choose < 0.1f) //no mediki
                {
                    Instantiate(medkitCabinates[0], go.transform.position , go.transform.rotation);
                    medCount += 1;
                }
                else if (choose > 0.9f) //2 medkits
                {
                    Instantiate(medkitCabinates[2], go.transform.position , go.transform.rotation);
                    medCount += 1;
                }
                else //1 medkit
                {
                    Instantiate(medkitCabinates[1], go.transform.position , go.transform.rotation);
                    medCount += 1;
                }
            }
        }
    }

    private void ApplyInfection()
    {
        bool portalSpawned = false;
        bool first = true;
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Type3");
        foreach (GameObject go in gos)
        {
            if (first)
            {
               // Instantiate(infectionPrefabs[Random.Range(0, infectionPrefabs.Length)], go.transform.position + new Vector3(0, 0.2f, 0), go.transform.rotation);
                first = false;
            } else 
            if (Random.Range(0.0f, 1.0f) > 0.6f)
            {
                Instantiate(infectionPrefabs[Random.Range(0, infectionPrefabs.Length)], go.transform.position + new Vector3(0, 0.2f, 0), go.transform.rotation);
            } else if (portalSpawned == false && go.tag != "SpawnRoom" && (Random.Range(0.0f, 1.0f) < 0.2))
            {
                Instantiate(portalPrefab, go.transform.position + new Vector3(0, 0, 0), go.transform.rotation);
                portalSpawned = true;
            }
        }
    }

    //private void addPipes()
    //{
    //    GameObject[] gos = GameObject.FindGameObjectsWithTag("Type1");
    //    foreach(GameObject go in gos)
    //    {
    //        Instantiate(pipesPrefab, go.transform.position, go.transform.rotation);
    //    }
    //    GameObject[] gos1 = GameObject.FindGameObjectsWithTag("Type4");
    //    foreach (GameObject go1 in gos1)
    //    {
    //        Instantiate(pipesPrefab, go1.transform.position, go1.transform.rotation);
    //    }
    //}

    // place minimap tiles 
    private void GenMiniMap()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Type1");
        foreach (GameObject go in gos)
        {
            Instantiate(MiniMapTypes[0], go.transform.position, go.transform.rotation);
        }
        gos = GameObject.FindGameObjectsWithTag("Type2");
        foreach (GameObject go in gos)
        {
            Instantiate(MiniMapTypes[1], go.transform.position, go.transform.rotation);
        }
        gos = GameObject.FindGameObjectsWithTag("Type3");
        foreach (GameObject go in gos)
        {
            Instantiate(MiniMapTypes[2], go.transform.position, go.transform.rotation);
        }
        gos = GameObject.FindGameObjectsWithTag("Type4");
        foreach (GameObject go in gos)
        {
            Instantiate(MiniMapTypes[3], go.transform.position, go.transform.rotation);
        }
    }



    private LevelCell GenCell(IntVector2 coordinates)
    {
        if (firstCell)
        {
            LevelCell newCell;
            if (SceneManager.GetActiveScene().name == "Level1")
            {
                newCell = Instantiate(spawnCellPrefab) as LevelCell;
            } else
            {
                newCell = Instantiate(spawnCellPrefab) as LevelCell;
            }
            firstCell = false;
            cells[coordinates.x, coordinates.z] = newCell;
            newCell.coordinates = coordinates;
            newCell.name = "Spawn Cell " + coordinates.x + "," + coordinates.z;
            newCell.gameObject.tag = "SpawnRoom";
            newCell.transform.parent = transform;
            newCell.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 0f, coordinates.z - size.z * 0.5f + 0.5f);
            GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 15f, coordinates.z - size.z * 0.5f + 0.5f);
            return newCell;
        }
        else
        {
            LevelCell newCell = Instantiate(cellPrefab) as LevelCell;
            cells[coordinates.x, coordinates.z] = newCell;
            newCell.coordinates = coordinates;
            newCell.name = "Level Cell " + coordinates.x + "," + coordinates.z;
            newCell.transform.parent = transform;
            newCell.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 0f, coordinates.z - size.z * 0.5f + 0.5f);
            return newCell;
        }
    }

    private LevelCell GenSpawnCell(IntVector2 coordinates)
    {
        LevelCell newCell = Instantiate(cellPrefab) as LevelCell;
        cells[coordinates.x, coordinates.z] = newCell;
        newCell.coordinates = coordinates;
        newCell.name = "Spawn Cell " + coordinates.x + "," + coordinates.z;
        newCell.transform.parent = transform;
        newCell.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 0f, coordinates.z - size.z * 0.5f + 0.5f);
        return newCell;
    }

    //produces random x and z coordinates 
    public IntVector2 RandomCoords
    {
        get
        {
            return new IntVector2(Random.Range(0, size.x), Random.Range(0, size.z));
        }
    }

    //checks to see if coordinates are located within the dungeon (size)
    public bool ContainsCoords(IntVector2 coordinate)
    {
        return coordinate.x >= 0 && coordinate.x < size.x && coordinate.z >= 0 && coordinate.z < size.z;
    }

    //adds the first cell the the list of cells
    private void DoFirstGenStep(List<LevelCell> activeCells)
    {
        LevelCell newCell = GenCell(RandomCoords);
        newCell.Initialize(GenRoom(-1));
        activeCells.Add(newCell);
    }

    //used to create actual rooms!
    private void GenPassageInRoom(LevelCell cell, LevelCell otherCell, GenerationDirection direction)
    {
        Passage passage = Instantiate(passagePrefab) as Passage;
        passage.InitializeEdges(cell, otherCell, direction);
        passage = Instantiate(passagePrefab) as Passage;
        passage.InitializeEdges(otherCell, cell, direction.GetOpposite());
        //check wether its connecting different rooms
        if (cell.room != otherCell.room)
        {
            Room roomToAssimilate = otherCell.room;
            cell.room.Assimilate(roomToAssimilate);
            rooms.Remove(roomToAssimilate);
            Destroy(roomToAssimilate);
        }
    }

    //if generation cannot move one step from current cell, remove cell from list and try again
    private void DoNextGenStep(List<LevelCell> activeCells)
    {
        int curIndex = activeCells.Count - 1;
        LevelCell curCell = activeCells[curIndex];
        if (curCell.IsFullyInitialized)
        {
            activeCells.RemoveAt(curIndex);
            return;
        }
        GenerationDirection direction = curCell.RandomUnitilalizedDirection;
        IntVector2 coordinates = curCell.coordinates + direction.ToIntVector2();
        if (ContainsCoords(coordinates))
        {
            LevelCell neighbour = GetCell(coordinates);
            if (neighbour == null) //check if no cell next to current cell and creates passage if there isnt 
            {
                neighbour = GenCell(coordinates);
                GenPassage(curCell, neighbour, direction);
                activeCells.Add(neighbour);
            }else if (curCell.room.settingsIndx == neighbour.room.settingsIndx) //removes walls inside of rooms 
            {
                GenPassageInRoom(curCell, neighbour, direction);
            }
            else //creates wall if there is 
            {
                GenWall(curCell, neighbour, direction);             
            }
        }
        else
        {
            GenWall(curCell, null, direction);
        }
    }

    private void GenPassage (LevelCell cell, LevelCell otherCell, GenerationDirection direction)
    {
        Passage prefab = Random.value < doorProb ? doorPrefab : passagePrefab;
        Passage passage = Instantiate(prefab) as Passage;
        passage.InitializeEdges(cell, otherCell, direction);
        passage = Instantiate(prefab) as Passage;
        if (passage is Door)
        {
            otherCell.Initialize(GenRoom(cell.room.settingsIndx));
        } else
        {
            otherCell.Initialize(cell.room);
        }
        passage.InitializeEdges(otherCell, cell, direction.GetOpposite()); 
    }

    private void GenWall (LevelCell cell, LevelCell otherCell, GenerationDirection direction)
    {
        Wall wall = Instantiate(wallPrefabs[Random.Range(0, wallPrefabs.Length)]) as Wall;
        wall.InitializeEdges(cell, otherCell, direction);
        if (otherCell != null)
        {
            wall = Instantiate(wallPrefabs[Random.Range(0, wallPrefabs.Length)]) as Wall;
            wall.InitializeEdges(otherCell, cell, direction.GetOpposite());
        }
    }

    //used for room assignment
    private List<Room> rooms = new List<Room>();

    private Room GenRoom (int indexToExclude)
    {
        Room newRoom = ScriptableObject.CreateInstance<Room>();
        newRoom.settingsIndx = Random.Range(0, roomSettings.Length);
        if (newRoom.settingsIndx == indexToExclude)
        {
            newRoom.settingsIndx = (newRoom.settingsIndx + 1) % roomSettings.Length; //% -  computes the remainder of a division
        }
        newRoom.settings = roomSettings[newRoom.settingsIndx];
        int roomCheck = newRoom.settingsIndx;
        if (roomCheck == 0)
        {
            mbC += 1;
        }
        if (roomCheck == 1)
        {
            mC += 1;
        }
        if (roomCheck == 2)
        {
            sC += 1;
        }
        if (roomCheck == 3)
        {
            dnC += 1;
        }
        rooms.Add(newRoom);
        return newRoom;
    }
}
