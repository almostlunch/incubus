﻿using UnityEngine;
using System.Collections;

public class PlayerLocation : MonoBehaviour {

    private LevelCell curCell;

    public void SetLocation(LevelCell cell)
    {
        if (curCell != null)
        {
            curCell.OnPlayerExited();
        }
        curCell = cell;
        curCell.OnPlayerEntered();
    }
}
