﻿using UnityEngine;
using System.Collections;

public class RotateSkyBox : MonoBehaviour {

        float curRot = 0;

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            curRot += 2 * Time.deltaTime;
            curRot %= 360;
            RenderSettings.skybox.SetFloat("_Rotation", curRot);
        }   
}
