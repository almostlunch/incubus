﻿using UnityEngine;
using System.Collections;

public class Wall : LevelCellEdge {
    public Transform wall;

    public override void InitializeEdges (LevelCell cell, LevelCell otherCell, GenerationDirection direction)
    {
        base.InitializeEdges(cell, otherCell, direction);
        //float test = Random.Range(0, 100);
        //if (test > 90)
        //{
        //    wall.GetComponent<Renderer>().material = cell.room.settings.wallMat2;
        //}
        //else
        //{
        //    wall.GetComponent<Renderer>().material = cell.room.settings.wallMat;
        //}
        wall.GetComponent<Renderer>().material = cell.room.settings.wallMat;
        gameObject.tag = cell.room.settings.wallType;
    }
}
