﻿using UnityEngine;
using System.Collections;

//abstract as instance of generatic LevelCellEdge should never be created
public abstract class LevelCellEdge : MonoBehaviour {

    //reference to the cell the edge belongs to and the cell its next to
    public LevelCell cell, otherCell;
    //record the cells orientation
    public GenerationDirection direction;

    //make edges children of their cells and place them in the same location (virtual to allow for overide in door class)
    public virtual void InitializeEdges(LevelCell cell, LevelCell otherCell, GenerationDirection direction)
    {
        this.cell = cell;
        this.otherCell = otherCell;
        this.direction = direction;
        cell.SetEdge(direction, this);
        transform.parent = cell.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = direction.Rotate();
    }

    //player enters/exits cells
    public virtual void OnPlayerEntered() { }
    public virtual void OnPlayerExited() { }
}
