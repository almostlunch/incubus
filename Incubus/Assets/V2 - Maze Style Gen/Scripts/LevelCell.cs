﻿using UnityEngine;
using System.Collections;

public class LevelCell : MonoBehaviour {

    //Used to add coords to each cell
    public IntVector2 coordinates;
    //used for room class
    public Room room;

    //used with isFullyInitialized & SetEdge to keep track of how often an edge is sent
    private int initializedEdgeCount;

    //store edges in an array
    private LevelCellEdge[] edges = new LevelCellEdge[GenerationDirections.NoOf];
    //get method
    public LevelCellEdge GetEdge(GenerationDirection direction)
    {
        return edges[(int)direction];
    }
    //set method
    public void SetEdge(GenerationDirection direction, LevelCellEdge edge)
    {
        edges[(int)direction] = edge;
        initializedEdgeCount += 1;
    }

    public bool IsFullyInitialized
    {
        get
        {
            return initializedEdgeCount == GenerationDirections.NoOf;
        }
    }

    //get unbiased random direction
    public GenerationDirection RandomUnitilalizedDirection
    {
        get
        {
            int noOfSkips = Random.Range(0, GenerationDirections.NoOf - initializedEdgeCount);
            for (int i = 0; i < GenerationDirections.NoOf; i++)
            {
                if (edges[i] == null)
                {
                    if (noOfSkips == 0)
                    {
                        return (GenerationDirection)i;
                    }
                    noOfSkips -= 1;
                }
            }
            throw new System.InvalidOperationException("Cell has no unused directions left.");
        }
    }

    //used to assign materials to rooms 
    public void Initialize (Room room)
    {
        room.Add(this);
        //float test = Random.Range(0, 100);
        //if (test > 80)
        //{
        //    transform.GetChild(0).GetComponent<Renderer>().material = room.settings.floorMat2;
        //}
        //else
        //{
        //    transform.GetChild(0).GetComponent<Renderer>().material = room.settings.floorMat;
        //}
        transform.GetChild(0).GetComponent<Renderer>().material = room.settings.floorMat;
        gameObject.tag = room.settings.roomType;
    }

    //used to load/unload cells
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    //push info when player enteres or exits a cell
    public void OnPlayerEntered()
    {
        room.Show();
        for (int i = 0; i < edges.Length; i++)
        {
            edges[i].OnPlayerEntered();
        }
    }
    public void OnPlayerExited()
    {
        room.Hide();
        for (int i = 0; i < edges.Length; i++)
        {
            edges[i].OnPlayerExited();
        }
    }
}
