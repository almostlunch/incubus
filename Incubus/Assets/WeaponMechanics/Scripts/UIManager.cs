﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private GameObject rifle;
    private GameObject shotgun;
    private GameObject pistol;

    private GameObject m_character;
    private GameObject m_jetpackText;
    private GameObject m_grenadeCount;
    private GameObject m_ammoText;
    private int activeWeapon;

    void Start()
    {
        rifle = gameObject.GetComponent<GunGO>().GetRifle();
        shotgun = gameObject.GetComponent<GunGO>().GetShotgun();
        pistol = gameObject.GetComponent<GunGO>().GetPistol();

        m_character = GameObject.FindGameObjectWithTag("Player");
        m_jetpackText = GameObject.FindGameObjectWithTag("JetpackText");
        m_grenadeCount = GameObject.FindGameObjectWithTag("GrenadeCount");
        m_ammoText = GameObject.FindGameObjectWithTag("AmmoText");
    }

    void Update()
    {
        //m_jetpackText.GetComponent<Text>().text = "Jetpack Charge: " + m_character.GetComponent<JetpackCharge>().GetJetpackCharge().ToString("F2");
        //m_grenadeCount.GetComponent<Text>().text = "Grenade Count: " + gameObject.GetComponent<GrenadeManager>().GetGrenadeCount().ToString();

        activeWeapon = gameObject.GetComponent<GunManager>().GetActiveWeapon();
        
        if (activeWeapon == 1)
        {
            //m_ammoText.GetComponent<Text>().text = "Energy: " + rifle.GetComponent<Rifle>().GetCurrEnergy().ToString() + " / " + rifle.GetComponent<Rifle>().GetMaxEnergy().ToString();
        }
        else if (activeWeapon == 2)
        {
            //m_ammoText.GetComponent<Text>().text = "Energy: " + shotgun.GetComponent<Shotgun>().GetCurrEnergy().ToString() + " / " + shotgun.GetComponent<Shotgun>().GetMaxEnergy().ToString();
        }
        else if (activeWeapon == 3)
        {
            //m_ammoText.GetComponent<Text>().text = "Energy: " + pistol.GetComponent<Pistol>().GetCurrEnergy().ToString() + " / " + pistol.GetComponent<Pistol>().GetMaxEnergy().ToString();
        }
    }
}