﻿using UnityEngine;
using System.Collections;

public class GrenadeManager : MonoBehaviour
{
    public GameObject camera;
    public GameObject grenade;

    private int grenadeCount;
    private bool canThrow;
    private float grenadeWait;

    void Start()
    {
        grenadeCount = 3;
        canThrow = true;
        grenadeWait = 1.0f;
    }

    void AddGrenade(int no)
    {
        grenadeCount += no;
    }

    public void ThrowGrenade()
    {
        if (grenadeCount > 0)
        {
            if (canThrow)
            {
                StartCoroutine(ThrowWait());

                Instantiate(grenade, camera.transform.position + camera.transform.forward, camera.transform.rotation * Quaternion.Euler(0.0f, 1.0f, 0.0f));

                grenadeCount -= 1;
            }
        }
    }

    IEnumerator ThrowWait()
    {
        canThrow = false;
        yield return new WaitForSeconds(grenadeWait);
        canThrow = true;
    }

    public int GetGrenadeCount()
    {
        return grenadeCount;
    }
}