﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Shotgun : Gun
{
    public AudioClip flameSound;
    public AudioClip grenadeSound;

    new void Start()
    {
        base.Start();

        //maxEnergy = 100;
        //currEnergy = 100;
        energyUse = 5;
        fireRate = 1.0f;
        damage = 5;
        inaccuracy = 0.07f;
        baseFireRate = 1.0f;
        baseDamage = 5;
        baseInaccuracy = 0.07f;
        inaccuracyLerp = 1.0f;
        inaccuracyMultiplier = 1.0f;
        noOfBulletsPerShot = 10;
        baseNoOfBulletsPerShot = 10;

        canShoot = true;

        skillTree = new Pair<string, int>[9];

        PopulateSkillTree();
    }

    new void Update()
    {
        base.Update();

        maxEnergy = WeaponEnergy.GetMaxEnergyShotgun();
        currEnergy = WeaponEnergy.GetCurrEnergyShotgun();

        damage = baseDamage + (skillTree[0].GetB());

        fireRate = baseFireRate - (skillTree[2].GetB() / 20.00f);

        inaccuracy = baseInaccuracy + (skillTree[5].GetB() / 100.00f) ;

        scriptManager.GetComponent<WeaponEnergy>().SetShotgunRegen(skillTree[8].GetB());
    }

    public override void AddEnergy(int e)
    {
        WeaponEnergy.AddEnergyShotgun(e);
    }

    protected override List<string> ProcStatus()
    {
        List<string> temp = new List<string>();

        if (skillTree[1].GetB() > 0)
        {
            if ((skillTree[1].GetB() * 5) > Random.Range(0, 100))
            {
                temp.Add("Cripple");
            }
        }

        if (skillTree[7].GetB() > 0)
        {
            if ((skillTree[1].GetB() * 10) > Random.Range(0, 100))
            {
                temp.Add("Fire");
            }
        }

        return temp;
    }

    protected override List<string> ProcBuff()
    {
        List<string> temp = new List<string>();

        if (skillTree[5].GetB() > 0)
        {
            for (int i = 0; i < skillTree[5].GetB(); i++)
            {
                if ((skillTree[5].GetB() * 5) > Random.Range(0, 100))
                {
                    temp.Add("MultiShot");
                }
            }
        }

        if (skillTree[4].GetB() > 0)
        {
            if (skillTree[4].GetB() * 5 > Random.Range(0, 100))
            {
                temp.Add("NotUseEnergy");
            }
        }

        return temp;
    }

    public void PopulateSkillTree()
    {
        skillTree[0] = new Pair<string, int>("Damage", 0);      //DONE
        skillTree[1] = new Pair<string, int>("ChanceToCrippleOnImpact", 0);     //DONE
        skillTree[2] = new Pair<string, int>("Firerate", 0);    //DONE

        //Grenade Launcher
        skillTree[3] = new Pair<string, int>("BiggerExplosion", 0);     //DONE
        skillTree[4] = new Pair<string, int>("ChanceToNotUseEnergy", 0);
        skillTree[5] = new Pair<string, int>("ClusterBomb", 0);     //DONE

        //Flame
        skillTree[6] = new Pair<string, int>("WiderSpread", 0);
        skillTree[7] = new Pair<string, int>("FireProcChance", 0);      //DONE
        skillTree[8] = new Pair<string, int>("EnergyRegen", 0);     //DONE
    }

    public void ShootFlame()
    {
        if (canShoot)
        {
            if (currEnergy > 0)
            {
                AddEnergy(-energyUse);

                    gameObject.GetComponent<AudioSource>().clip = flameSound;
                    gameObject.GetComponent<AudioSource>().Play();

                multiShot = 0;

                //Deals with procs for buffs
                foreach (string s in ProcBuff())
                {
                    if (s == "NotUseEnergy")
                    {
                        AddEnergy(energyUse);
                    }
                    else if (s == "MultiShot")
                    {
                        multiShot++;
                    }
                }

                for (int i = 0; i < noOfBulletsPerShot + multiShot; i++)
                {
                    Vector3 dir = m_camera.transform.rotation * Vector3.forward;
                    Vector3 inaccuracyVector = new Vector3(
                        Random.Range(-inaccuracy * inaccuracyLerp, inaccuracy * inaccuracyLerp),
                        Random.Range(-inaccuracy * inaccuracyLerp / 10, inaccuracy * inaccuracyLerp / 10),
                        Random.Range(-inaccuracy * inaccuracyLerp, inaccuracy * inaccuracyLerp));

                    RaycastHit[] hits;

                    hits = Physics.RaycastAll(m_camera.transform.position, dir + inaccuracyVector, 15.0f).OrderBy(h => h.distance).ToArray();

                    //If something it hit then tell the bullet trail to move towards the end point of the hit else give it an angle offset to make it look accurate
                    if (hits.Length != 0)
                    {
                        offsetTrailAngle = Angles(hits[0].point);

                        int goThrough;

                        if (penetrationNo == 1)
                            goThrough = 1;
                        else if (hits.Length > penetrationNo)
                            goThrough = penetrationNo;
                        else
                            goThrough = hits.Length;

                        for (int j = 0; j < goThrough; j++)
                        {
                            //Do stuff depending on what it hit
                            if (hits[j].collider.gameObject.tag == "Damageable")
                            {
                                hits[j].transform.gameObject.GetComponent<Damageable>().TakeDamage(damage);
                                scriptManager.GetComponent<HitMarker>().HitDamageable();

                                Instantiate(blood, hits[j].point, Quaternion.FromToRotation(Vector3.up, hits[j].normal));

                                //Deals with status effect procs on enemies
                                foreach (string s in ProcStatus())
                                {
                                    if (s == "Cripple")
                                    {
                                        hits[j].transform.gameObject.GetComponent<Damageable>().Cripple();
                                    }
                                    if (s == "Fire")
                                    {
                                        hits[j].transform.gameObject.GetComponent<Damageable>().AddFireStack(1);
                                    }
                                }
                            }
                            else if (hits[j].collider.gameObject.tag == "Wall" || hits[j].collider.gameObject.tag == "Floor")
                            {
                                //Instantiate(bulletImpact, hits[j].point + (hits[j].normal * 0.001f), Quaternion.FromToRotation(Vector3.up, hits[j].normal));
                                //Instantiate(smoke, hits[j].point + (hits[j].normal * 0.001f), Quaternion.Euler(0.0f, 0.0f, 0.0f));
                            }
                            else if (hits[0].collider.gameObject.tag == "Barrel" || hits[0].collider.gameObject.tag == "Pepe")
                            {
                                hits[0].collider.gameObject.GetComponent<Destoryable>().Wreck();
                            }
                        }

                        Instantiate(fireTrail, bulletLeave.transform.position, Quaternion.LookRotation(dir + inaccuracyVector));
                    }
                    else
                    {
                        offsetTrailAngle = 1.0f;
                        Instantiate(fireTrail, bulletLeave.transform.position, Quaternion.LookRotation(dir + inaccuracyVector) * Quaternion.Euler(0, -offsetTrailAngle, 0));
                    }
                }

                //Muzzle flash
                //GameObject mFlash = (GameObject)Instantiate(muzzleFlash, bulletLeave.transform.position, bulletLeave.transform.rotation * Quaternion.Euler(0, 0, Random.Range(0, 360)));
                //mFlash.GetComponent<FollowBulletLeave>().SetBulletLeave(bulletLeave);

                //Smoke
                Instantiate(smoke, bulletLeave.transform.position, Quaternion.Euler(0.0f, 0.0f, 0.0f));

                canShoot = false;

                //Stuff for other scripts to make it more "gun-like"
                //parentGun.GetComponent<WeaponMovement>().ShotFalse();
                parentGun.GetComponent<WeaponMovement>().ShotTrue();

                if (parentGunRot != null)
                {
                    parentGunRot.GetComponent<WeaponRotate>().ShotTrue();
                }

                foreach (GameObject go in movingParts)
                {
                    //go.GetComponent<MovingParts>().ShotFalse();
                    go.GetComponent<MovingParts>().ShotTrue();
                }

                theCamera.GetComponent<CameraRecoil>().ShotTrue();
            }
        }
    }

    public void ShootGrenade()
    {
        if (canShoot)
        {
            if (currEnergy > 0)
            {
                //currEnergy -= energyUse;
                AddEnergy(-energyUse);

                gameObject.GetComponent<AudioSource>().clip = grenadeSound;
                gameObject.GetComponent<AudioSource>().Play();

                multiShot = 0;

                //Deals with procs for buffs
                foreach (string s in ProcBuff())
                {
                    if (s == "NotUseEnergy")
                    {
                        AddEnergy(energyUse);
                    }
                    else if (s == "MultiShot")
                    {
                        multiShot++;
                    }
                }
                


                for (int i = 0; i < noOfBulletsPerShot + multiShot; i++)
                {
                    Vector3 dir = m_camera.transform.rotation * Vector3.forward;
                    Vector3 inaccuracyVector = new Vector3(
                        Random.Range(-inaccuracy, inaccuracy),
                        Random.Range(-inaccuracy, inaccuracy),
                        Random.Range(-inaccuracy, inaccuracy));

                    Instantiate(grenade, bulletLeave.transform.position, Quaternion.LookRotation(dir + inaccuracyVector) * Quaternion.Euler(0, -offsetTrailAngle, 0));
                }    

                //Muzzle flash
                GameObject mFlash = (GameObject)Instantiate(muzzleFlash, bulletLeave.transform.position, bulletLeave.transform.rotation * Quaternion.Euler(0, 0, Random.Range(0, 360)));
                mFlash.GetComponent<FollowBulletLeave>().SetBulletLeave(bulletLeave);

                //Smoke
                Instantiate(smoke, bulletLeave.transform.position, Quaternion.Euler(0.0f, 0.0f, 0.0f));

                canShoot = false;

                //Stuff for other scripts to make it more "gun-like"
                //parentGun.GetComponent<WeaponMovement>().ShotFalse();
                parentGun.GetComponent<WeaponMovement>().ShotTrue();

                if (parentGunRot != null)
                {
                    parentGunRot.GetComponent<WeaponRotate>().ShotTrue();
                }

                foreach (GameObject go in movingParts)
                {
                    //go.GetComponent<MovingParts>().ShotFalse();
                    go.GetComponent<MovingParts>().ShotTrue();
                }

                theCamera.GetComponent<CameraRecoil>().ShotTrue();
            }
        }
    }

    public void Augment(string s)
    {
        if (s == "Flame")
        {
            energyUse = 1;
            baseFireRate = 0.2f;
            baseDamage = 5;
            baseInaccuracy = 0.07f;
        }
        else if (s == "Grenade")
        {
            baseFireRate = 1.0f;
            baseDamage = 5;
            baseInaccuracy = 0.02f;
            noOfBulletsPerShot = 1;
        }
    }

    public void SetIsShotgunAug(string s)
    {
        if (s == "Flame")
            isShotgunFlame = true;
        else if (s == "Grenade")
            isShotgunGrenade = true;
    }

    public bool GetIsShotgunAug(string s)
    {
        if (s == "Flame")
            return isShotgunFlame;
        else if (s == "Grenade")
            return isShotgunGrenade;
        else
            return false;
    }

    public int GetDamage()
    {
        return damage;
    }
}