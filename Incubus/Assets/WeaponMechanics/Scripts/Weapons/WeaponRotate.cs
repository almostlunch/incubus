﻿using UnityEngine;
using System.Collections;

public class WeaponRotate : MonoBehaviour
{
    public GameObject scriptManager;

    private Quaternion startRot;
    private Quaternion recoilRot;
    private Quaternion varRot;
    private float rotLerpNo;
    private float rotateMultiplier;

    private bool shot;

    void Start()
    {
        startRot = transform.localRotation;
    }

    void Update()
    {
        WeaponRotation();

        if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 1)
        {
            varRot = Quaternion.Euler(-3.0f, 0.0f, 0.0f);
            recoilRot = startRot * varRot;
            rotateMultiplier = 0.5f;
        }
        else if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 2)
        {
            if (scriptManager.GetComponent<GunGO>().GetShotgun().GetComponent<Shotgun>().GetIsShotgunAug("Flame"))
            {
                varRot = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                recoilRot = startRot * varRot;
                rotateMultiplier = 2.0f;
            }
            else
            {
                varRot = Quaternion.Euler(-20.0f, 0.0f, 0.0f);
                recoilRot = startRot * varRot;
                rotateMultiplier = 2.0f;
            }
        }
        else if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 3)
        {
            varRot = Quaternion.Euler(-20.0f, 0.0f, 0.0f);
            recoilRot = startRot * varRot;
            rotateMultiplier = 1.0f;
        }

        //shot = false;
    }

    void WeaponRotation()
    {
        if (shot)
        {
            rotLerpNo += Time.deltaTime * 40 * rotateMultiplier;

            if (rotLerpNo > 1.0f)
            {
                rotLerpNo = 1.0f;
            }

            shot = false;
        }
        else
        {
            if (rotLerpNo < 0.0f)
            {
                rotLerpNo = 0.0f;
            }
            else
            {
                rotLerpNo -= Time.deltaTime * 3;
            }
        }

        transform.localRotation = Quaternion.Slerp(startRot, recoilRot, rotLerpNo);
    }

    public void ShotTrue()
    {
        shot = true;
    }

    public void ShotFalse()
    {
        shot = false;
    }
}