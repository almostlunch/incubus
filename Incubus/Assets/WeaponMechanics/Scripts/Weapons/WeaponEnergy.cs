﻿using UnityEngine;
using System.Collections;

public class WeaponEnergy : MonoBehaviour
{
    //Hue value start = 100, end = 0. S = 1. V/B = 1.5.
    public GameObject rifleGlow;
    public GameObject shotgunGlow;

    private static int rifleMaxEnergy;
    private static int rifleCurrEnergy;
    private static int shotgunMaxEnergy;
    private static int shotgunCurrEnergy;

    private const float RIFLE_CHARGE_TIMER = 0.5f;
    private const float SHOTGUN_CHARGE_TIMER = 1.5f;
    private float rifleChargeTimerMultiplier = 0.0f;
    private float shotgunChargeTimerMultiplier = 0.0f;

    private float rifleChargeTimer;
    private float shotgunChargeTimer;

    void Start()
    {
        rifleMaxEnergy = 100;
        rifleCurrEnergy = 100;
        shotgunMaxEnergy = 100;
        shotgunCurrEnergy = 100;

        rifleChargeTimer = RIFLE_CHARGE_TIMER - rifleChargeTimerMultiplier;
        shotgunChargeTimer = SHOTGUN_CHARGE_TIMER - shotgunChargeTimerMultiplier;
    }

    void Update()
    {
        if (rifleCurrEnergy < rifleMaxEnergy)
        {
            rifleChargeTimer -= Time.deltaTime;

            if (rifleChargeTimer <= 0.0f)
            {
                rifleCurrEnergy += 1;
                rifleChargeTimer = RIFLE_CHARGE_TIMER - rifleChargeTimerMultiplier;
            }
        }

        if (shotgunCurrEnergy < shotgunMaxEnergy)
        {
            shotgunChargeTimer -= Time.deltaTime;

            if (shotgunChargeTimer <= 0.0f)
            {
                shotgunCurrEnergy += 1;
                shotgunChargeTimer = SHOTGUN_CHARGE_TIMER - shotgunChargeTimerMultiplier;
            }
        }

        ChangeGlow();
    }

    void ChangeGlow()
    {
        Color temp = new Color(1.5f - (rifleCurrEnergy * 1.5f) / 100, (rifleCurrEnergy * 1.5f) / 100, 0.0f) * 5.0f;
        if (temp.r > 2.0f)
            temp.r = 2.0f;
        if (temp.g > 1.5f)
            temp.g = 1.5f;
        
        rifleGlow.GetComponent<Renderer>().material.SetColor("_EmissionColor", temp);

        temp = new Color(1.5f - (shotgunCurrEnergy * 1.5f) / 100, (shotgunCurrEnergy * 1.5f) / 100, 0.0f) * 5.0f;
        if (temp.r > 2.0f)
            temp.r = 2.0f;
        if (temp.g > 1.5f)
            temp.g = 1.5f;

        shotgunGlow.GetComponent<Renderer>().material.SetColor("_EmissionColor", temp);
    }

    public static void SetMaxEnergyRifle(int energy)
    {
        rifleMaxEnergy = energy;
    }

    public static void SetCurrEnergyRifle(int energy)
    {
        rifleCurrEnergy = energy;
    }

    public static int GetMaxEnergyRifle()
    {
        return rifleMaxEnergy;
    }

    public static int GetCurrEnergyRifle()
    {
        return rifleCurrEnergy;
    }

    public static void SetMaxEnergyShotgun(int energy)
    {
        shotgunMaxEnergy = energy;
    }

    public static void SetCurrEnergyShotgun(int energy)
    {
        shotgunCurrEnergy = energy;
    }

    public static int GetMaxEnergyShotgun()
    {
        return shotgunMaxEnergy;
    }

    public static int GetCurrEnergyShotgun()
    {
        return shotgunCurrEnergy;
    }

    public static void AddEnergyRifle(int e)
    {
        rifleCurrEnergy += e;

        if (rifleCurrEnergy > rifleMaxEnergy)
        {
            rifleCurrEnergy = rifleMaxEnergy;
        }
    }

    public static void AddEnergyShotgun(int e)
    {
        shotgunCurrEnergy += e;

        if (shotgunCurrEnergy > shotgunMaxEnergy)
        {
            shotgunCurrEnergy = shotgunMaxEnergy;
        }
    }

    public void SetRifleRegen(int no)
    {
        rifleChargeTimerMultiplier = no / 40;
    }

    public void SetShotgunRegen(int no)
    {
        shotgunChargeTimerMultiplier = no / 10;
    }
}