﻿using UnityEngine;
using System.Collections;

public class WeaponColourSwap : MonoBehaviour
{
    public GameObject[] rifleParts;

    public Material dmrMain;
    public Material dmrDark;

    public Material lmgMain;
    public Material lmgDark;

    public void ChangeToDMR()
    {
        foreach (GameObject go in rifleParts)
        {
            if (go.name == "Gun")
                go.GetComponent<Renderer>().material = dmrMain;
            else
                go.GetComponent<Renderer>().material = dmrDark;
        }
    }

    public void ChangeToLMG()
    {
        foreach (GameObject go in rifleParts)
        {
            if (go.name == "Gun")
                go.GetComponent<Renderer>().material = lmgMain;
            else
                go.GetComponent<Renderer>().material = lmgDark;
        }
    }
}