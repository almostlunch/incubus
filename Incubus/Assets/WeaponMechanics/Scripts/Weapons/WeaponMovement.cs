﻿using UnityEngine;
using System.Collections;

public class WeaponMovement : MonoBehaviour
{
    public GameObject scriptManager;

    private Vector3 startPos;
    private Vector3 recoilPos;
    private Vector3 varPos;
    private float lerpNo;
    private bool shot;

    private float recoilMultiplier;

    void Start()
    {
        lerpNo = 0.0f;
        shot = false;

        startPos = transform.localPosition;
    }

    void Update ()
    {
        WeaponSway();
        WeaponRecoil();

        if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 1)
        {
            recoilMultiplier = 1.0f;
            varPos = new Vector3(0.0f, 0.03f, -0.15f);
        }
        else if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 2)
        {
            if (scriptManager.GetComponent<GunGO>().GetShotgun().GetComponent<Shotgun>().GetIsShotgunAug("Flame"))
            {
                recoilMultiplier = 0.5f;
                varPos = new Vector3(0.0f, 0.00f, -0.00f);
            }
            else
            {
                recoilMultiplier = 2.0f;
                varPos = new Vector3(0.0f, 0.03f, -0.25f);
            }
        }
        else if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 3)
        {
            recoilMultiplier = 0.5f;
            varPos = new Vector3(0.0f, 0.03f, -0.15f);
        }

        recoilPos = startPos + varPos;

        shot = false;
    }

    void WeaponSway()
    {
        float factorX = (Input.GetAxis("Mouse Y"));
        float factorY = (Input.GetAxis("Mouse X"));

        transform.localRotation = Quaternion.Slerp(transform.localRotation,
            Quaternion.Euler(factorX, 0.0f, -factorY),
            Time.deltaTime * 3);
    }

    public void WeaponRecoil()
    {
        if (shot)
        {
            lerpNo += Time.deltaTime * 40 * recoilMultiplier;

            if (lerpNo > 1.0f)
            {
                //shot = false;
                lerpNo = 1.0f;
            }
        }
        else
        {
            if (lerpNo < 0.0f)
            {
                lerpNo = 0.0f;
            }
            else
            {
                lerpNo -= Time.deltaTime * 4;
            }
        }

        //shot = false;

        transform.localPosition = Vector3.Lerp(startPos, recoilPos, lerpNo);
    }

    public void ShotTrue()
    {
        shot = true;
    }
    
    public void ShotFalse()
    {
        shot = false;
    }
}
