﻿using UnityEngine;
using System.Collections;

public class CameraRecoil : MonoBehaviour
{
    public GameObject scriptManager;
    private float lerpNo;
    private Quaternion startPos;
    private Quaternion endPos;

    private bool shot;

    private float recoil;
    private float recoilMultiplier;

    void Start()
    {
        startPos = transform.localRotation;
        endPos = transform.localRotation * Quaternion.Euler(-15.0f, 0.0f, 0.0f);

        lerpNo = 0.0f;
        shot = false;

        recoil = 0.0f;
    }

    void Update()
    {
        /*if (shot)
        {
            lerpNo += Time.deltaTime * 100;
        }
        else
        {
            if (lerpNo > 0.0f)
            {
                lerpNo -= Time.deltaTime;
            }
        }

        shot = false;

        if (lerpNo > 1.0f)
        {
            lerpNo = 1.0f;
        }

        transform.localRotation *= Quaternion.Slerp(startPos, Quaternion.Euler(-20.0f, 0.0f, 0.0f), lerpNo);*/

        if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 1)
        {
            if (scriptManager.GetComponent<GunGO>().GetRifle().GetComponent<Rifle>().GetIsRifleAug("LMG"))
                recoilMultiplier = 0.7f;
            else if (scriptManager.GetComponent<GunGO>().GetRifle().GetComponent<Rifle>().GetIsRifleAug("DMR"))
                recoilMultiplier = 2.5f;
            else
                recoilMultiplier = 1.2f;
        }
        else if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 2)
        {
            if (scriptManager.GetComponent<GunGO>().GetShotgun().GetComponent<Shotgun>().GetIsShotgunAug("Flame"))
                recoilMultiplier = 0.0f;
            else if (scriptManager.GetComponent<GunGO>().GetShotgun().GetComponent<Shotgun>().GetIsShotgunAug("Grenade"))
                recoilMultiplier = 2.5f;
            else
                recoilMultiplier = 5.0f;
        }
        else if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 3)
        {
            recoilMultiplier = 1.0f;
        }
    }

    public void ShotTrue()
    {
        shot = true;
    }

    public float RecoilCalc()
    {
        if (shot)
        {
            recoil += Time.deltaTime * 70 * recoilMultiplier;
        }
        else
        {
            if (recoil > 0.0f)
            {
                recoil -= Time.deltaTime * 10;

                if (recoil < 0.0f)
                {
                    recoil = 0.0f;
                }
            }
        }

        shot = false;

        if (recoil > 5.0f)
        {
            recoil = 5.0f;
        }

        return recoil;
    }
}