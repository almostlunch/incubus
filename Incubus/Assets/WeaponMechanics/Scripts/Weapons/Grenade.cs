﻿using UnityEngine;
using System.Collections;

public class Grenade : MonoBehaviour
{
    public GameObject explosionParticle;
    public GameObject scriptManager;

    private int damage;
    private float speed;
    private float explosionTimer;
    private float explosionPower;
    private float explosionRadius;
    private float explosionRadiusVar;

    void Start()
    {
        scriptManager = GameObject.FindGameObjectWithTag("ScriptManager");
        explosionRadiusVar = scriptManager.GetComponent<GunGO>().GetShotgun().GetComponent<Shotgun>().GetSkillTree()[4].GetB();
        damage = scriptManager.GetComponent<GunGO>().GetShotgun().GetComponent<Shotgun>().GetDamage();
        speed = 5.0f;
        explosionTimer = 3.0f;
        explosionPower = 1000.0f;
        explosionRadius = 10.0f;
        this.GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.Impulse);
    }

    void Update()
    {
        explosionTimer -= Time.deltaTime;

        if (explosionTimer <= 0.0f)
        {
            Explode();
        }
    }

    void Explode()
    {
        Instantiate(explosionParticle, transform.position, transform.rotation);

        Collider[] objectsInRange = Physics.OverlapSphere(transform.position, explosionRadius + explosionRadiusVar);

        foreach (Collider hit in objectsInRange)
        {
            Rigidbody rb = null;

            if (hit.GetComponent<Rigidbody>() != null)
                rb = hit.GetComponent<Rigidbody>();

            if (rb != null && rb.gameObject.tag != "Grenade")
            {
                rb.AddExplosionForce(explosionPower, transform.position, explosionRadius + explosionRadiusVar);
            }

            if (hit.gameObject.GetComponent<Damageable>() != null)
                hit.gameObject.GetComponent<Damageable>().TakeDamage(damage);

            if (hit.gameObject.tag == "Barrel" || hit.gameObject.tag == "Pepe")
            {
                hit.gameObject.GetComponent<Destoryable>().Wreck();
            }
        }

        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision c)
    {
        if (explosionTimer < 2.75f)
            Explode();
    }
}