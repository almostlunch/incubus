﻿using UnityEngine;
using System.Collections;

public class Pair<A, B>
{
    private A key;
    private B value;

    public Pair(A a, B b)
    {
        this.key = a;
        this.value = b;
    }

    public A GetA()
    {
        return key;
    }

    public B GetB()
    {
        return value;
    }

    public void SetA(A a)
    {
        this.key = a;
    }

    public void SetB(B b)
    {
        this.value = b;
    }
}
