﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HitMarker : MonoBehaviour
{
    public GameObject hitM;

    private float hitMarkerAlpha = 0.0f;

    void Update()
    {
        if (hitMarkerAlpha > 0.0f)
        {
            hitMarkerAlpha -= Time.deltaTime * 4;
        }

        if (hitMarkerAlpha < 0.0f)
            hitMarkerAlpha = 0.0f;

        if(hitM != null)
            hitM.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, hitMarkerAlpha);
    }

    public void HitDamageable()
    {
        hitMarkerAlpha = 1.0f;
    }
}