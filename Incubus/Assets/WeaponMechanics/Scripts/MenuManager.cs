﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public GameObject menuCanvas;
    public GameObject exitCanvas;

    private Button exitResume;
    private Button exitInstructions;
    private Button exitOptions;
    private Button exitQuit;
    private Image exitImgInstructions;
    private Text exitTxtInstructions;

    private GameObject rifle;
    private GameObject shotgun;
    private GameObject pistol;

    private Text rifleLevel;
    private Text shotgunLevel;
    private Text pistolLevel;

    private Text rifleSkillOneLevel;
    private Text rifleSkillTwoLevel;
    private Text rifleSkillThreeLevel;
    private Text rifleSkillFourLevel;
    private Text rifleSkillFiveLevel;
    private Text rifleSkillSixLevel;
    private Text rifleSkillSevenLevel;
    private Text rifleSkillEightLevel;
    private Text rifleSkillNineLevel;

    private Button rifleSkillOneBtn;
    private Button rifleSkillTwoBtn;
    private Button rifleSkillThreeBtn;
    private Button rifleSkillFourBtn;
    private Button rifleSkillFiveBtn;
    private Button rifleSkillSixBtn;
    private Button rifleSkillSevenBtn;
    private Button rifleSkillEightBtn;
    private Button rifleSkillNineBtn;

    private Text shotgunSkillOneLevel;
    private Text shotgunSkillTwoLevel;
    private Text shotgunSkillThreeLevel;
    private Text shotgunSkillFourLevel;
    private Text shotgunSkillFiveLevel;
    private Text shotgunSkillSixLevel;
    private Text shotgunSkillSevenLevel;
    private Text shotgunSkillEightLevel;
    private Text shotgunSkillNineLevel;

    private Button shotgunSkillOneBtn;
    private Button shotgunSkillTwoBtn;
    private Button shotgunSkillThreeBtn;
    private Button shotgunSkillFourBtn;
    private Button shotgunSkillFiveBtn;
    private Button shotgunSkillSixBtn;
    private Button shotgunSkillSevenBtn;
    private Button shotgunSkillEightBtn;
    private Button shotgunSkillNineBtn;

    private Text pistolSkillOneLevel;
    private Text pistolSkillTwoLevel;
    private Text pistolSkillThreeLevel;
    private Text pistolSkillFourLevel;
    private Text pistolSkillFiveLevel;
    private Text pistolSkillSixLevel;

    private Button pistolSkillOneBtn;
    private Button pistolSkillTwoBtn;
    private Button pistolSkillThreeBtn;
    private Button pistolSkillFourBtn;
    private Button pistolSkillFiveBtn;
    private Button pistolSkillSixBtn;

    private Button rifleAugmentLMG;
    private Button rifleAugmentDMR;

    private Button shotgunAugmentGrenade;
    private Button shotgunAugmentFlame;

    private bool isMenuOpen = false;
    private bool isExitOpen = false;
    private bool rifleAug = false;
    private bool shotgunAug = false;

    void Start()
    {
        rifle = gameObject.GetComponent<GunGO>().GetRifle();
        shotgun = gameObject.GetComponent<GunGO>().GetShotgun();
        pistol = gameObject.GetComponent<GunGO>().GetPistol();

        for (int i = 0; i < exitCanvas.transform.childCount; i++)
        {
            if (exitCanvas.transform.GetChild(i).name == "btnResume")
                exitResume = exitCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (exitCanvas.transform.GetChild(i).name == "btnInstructions")
                exitInstructions = exitCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (exitCanvas.transform.GetChild(i).name == "btnOptions")
                exitOptions = exitCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (exitCanvas.transform.GetChild(i).name == "btnQuit")
                exitQuit = exitCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (exitCanvas.transform.GetChild(i).name == "imgInstructions")
                exitImgInstructions = exitCanvas.transform.GetChild(i).GetComponent<Image>();
            else if (exitCanvas.transform.GetChild(i).name == "txtInstructions")
                exitTxtInstructions = exitCanvas.transform.GetChild(i).GetComponent<Text>();
        }

        for (int i = 0; i < menuCanvas.transform.childCount; i++)
        {
            if (menuCanvas.transform.GetChild(i).name == "rifleLevel")
                rifleLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "btnrifleSkillOne")
                rifleSkillOneBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnrifleSkillTwo")
                rifleSkillTwoBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnrifleSkillThree")
                rifleSkillThreeBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnrifleSkillFour")
                rifleSkillFourBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnrifleSkillFive")
                rifleSkillFiveBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnrifleSkillSix")
                rifleSkillSixBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnrifleSkillSeven")
                rifleSkillSevenBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnrifleSkillEight")
                rifleSkillEightBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnrifleSkillNine")
                rifleSkillNineBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "rifleSkillOneLvl")
                rifleSkillOneLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "rifleSkillTwoLvl")
                rifleSkillTwoLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "rifleSkillThreeLvl")
                rifleSkillThreeLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "rifleSkillFourLvl")
                rifleSkillFourLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "rifleSkillFiveLvl")
                rifleSkillFiveLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "rifleSkillSixLvl")
                rifleSkillSixLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "rifleSkillSevenLvl")
                rifleSkillSevenLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "rifleSkillEightLvl")
                rifleSkillEightLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "rifleSkillNineLvl")
                rifleSkillNineLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "RifleAugment_LMG")
                rifleAugmentLMG = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "RifleAugment_DMR")
                rifleAugmentDMR = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "shotgunLevel") //START
                shotgunLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "btnshotgunSkillOne")
                shotgunSkillOneBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnshotgunSkillTwo")
                shotgunSkillTwoBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnshotgunSkillThree")
                shotgunSkillThreeBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnshotgunSkillFour")
                shotgunSkillFourBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnshotgunSkillFive")
                shotgunSkillFiveBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnshotgunSkillSix")
                shotgunSkillSixBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnshotgunSkillSeven")
                shotgunSkillSevenBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnshotgunSkillEight")
                shotgunSkillEightBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnshotgunSkillNine")
                shotgunSkillNineBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "shotgunSkillOneLvl")
                shotgunSkillOneLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "shotgunSkillTwoLvl")
                shotgunSkillTwoLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "shotgunSkillThreeLvl")
                shotgunSkillThreeLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "shotgunSkillFourLvl")
                shotgunSkillFourLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "shotgunSkillFiveLvl")
                shotgunSkillFiveLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "shotgunSkillSixLvl")
                shotgunSkillSixLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "shotgunSkillSevenLvl")
                shotgunSkillSevenLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "shotgunSkillEightLvl")
                shotgunSkillEightLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "shotgunSkillNineLvl")
                shotgunSkillNineLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "ShotgunAugment_Grenade")
                shotgunAugmentGrenade = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "ShotgunAugment_Flame")
                shotgunAugmentFlame = menuCanvas.transform.GetChild(i).GetComponent<Button>(); //END
            else if (menuCanvas.transform.GetChild(i).name == "pistolLevel")
                pistolLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "btnpistolSkillOne")
                pistolSkillOneBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnpistolSkillTwo")
                pistolSkillTwoBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnpistolSkillThree")
                pistolSkillThreeBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnpistolSkillFour")
                pistolSkillFourBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnpistolSkillFive")
                pistolSkillFiveBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "btnpistolSkillSix")
                pistolSkillSixBtn = menuCanvas.transform.GetChild(i).GetComponent<Button>();
            else if (menuCanvas.transform.GetChild(i).name == "pistolSkillOneLvl")
                pistolSkillOneLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "pistolSkillTwoLvl")
                pistolSkillTwoLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "pistolSkillThreeLvl")
                pistolSkillThreeLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "pistolSkillFourLvl")
                pistolSkillFourLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "pistolSkillFiveLvl")
                pistolSkillFiveLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
            else if (menuCanvas.transform.GetChild(i).name == "pistolSkillSixLvl")
                pistolSkillSixLevel = menuCanvas.transform.GetChild(i).GetComponent<Text>();
        }

        rifleSkillFourBtn.interactable = false;
        rifleSkillFiveBtn.interactable = false;
        rifleSkillSixBtn.interactable = false;
        rifleSkillSevenBtn.interactable = false;
        rifleSkillEightBtn.interactable = false;
        rifleSkillNineBtn.interactable = false;

        shotgunSkillFourBtn.interactable = false;
        shotgunSkillFiveBtn.interactable = false;
        shotgunSkillSixBtn.interactable = false;
        shotgunSkillSevenBtn.interactable = false;
        shotgunSkillEightBtn.interactable = false;
        shotgunSkillNineBtn.interactable = false;

        rifleAugmentLMG.interactable = false;
        rifleAugmentDMR.interactable = false;

        shotgunAugmentFlame.interactable = false;
        shotgunAugmentGrenade.interactable = false;

        CloseInstructions();
    }

    void Update()
    {
        if (rifle.GetComponent<Gun>().GetLevel() >= 10 && !rifleAug)
        {
            rifleAugmentLMG.interactable = true;
            rifleAugmentDMR.interactable = true;
        }

        if (shotgun.GetComponent<Gun>().GetLevel() >= 10 && !shotgunAug)
        {
            shotgunAugmentFlame.interactable = true;
            shotgunAugmentGrenade.interactable = true;
        }
    }

    public void AugmentLMG()
    {
        rifleAug = true;

        rifle.GetComponent<Rifle>().SetIsRifleAug("LMG");
        rifle.GetComponent<Rifle>().Augment("LMG");

        rifleSkillFourBtn.interactable = true;
        rifleSkillFiveBtn.interactable = true;
        rifleSkillSixBtn.interactable = true;

        rifleAugmentLMG.interactable = false;
        rifleAugmentDMR.interactable = false;

        //gameObject.GetComponent<WeaponColourSwap>().ChangeToLMG();
    }

    public void AugmentDMR()
    {
        rifleAug = true;

        rifle.GetComponent<Rifle>().SetIsRifleAug("DMR");
        rifle.GetComponent<Rifle>().Augment("DMR");

        rifleSkillSevenBtn.interactable = true;
        rifleSkillEightBtn.interactable = true;
        rifleSkillNineBtn.interactable = true;

        rifleAugmentLMG.interactable = false;
        rifleAugmentDMR.interactable = false;
        //gameObject.GetComponent<WeaponColourSwap>().ChangeToDMR();
    }

    public void AugmentFlame()
    {
        shotgunAug = true;
        shotgun.GetComponent<Shotgun>().SetIsShotgunAug("Flame");
        shotgun.GetComponent<Shotgun>().Augment("Flame");

        shotgunSkillSevenBtn.interactable = true;
        shotgunSkillEightBtn.interactable = true;
        shotgunSkillNineBtn.interactable = true;

        shotgunAugmentFlame.interactable = false;
        shotgunAugmentGrenade.interactable = false;
    }

    public void AugmentGrenade()
    {
        shotgunAug = true;
        shotgun.GetComponent<Shotgun>().SetIsShotgunAug("Grenade");
        shotgun.GetComponent<Shotgun>().Augment("Grenade");

        shotgunSkillFourBtn.interactable = true;
        shotgunSkillFiveBtn.interactable = true;
        shotgunSkillSixBtn.interactable = true;

        shotgunAugmentFlame.interactable = false;
        shotgunAugmentGrenade.interactable = false;
    }

    public void OpenExit()
    {
        if (isMenuOpen)
            CloseMenu();

        isExitOpen = true;

        Time.timeScale = 0.0f;

        exitCanvas.SetActive(true);

        Cursor.lockState = CursorLockMode.None;
    }

    public void CloseExit()
    {
        isExitOpen = false;

        Time.timeScale = 1.0f;

        CloseInstructions();

        exitCanvas.SetActive(false);

        Cursor.lockState = CursorLockMode.Locked;
    }

    public void OpenInstructions()
    {
        exitImgInstructions.gameObject.SetActive(true);
        exitTxtInstructions.gameObject.SetActive(true);
    }

    public void CloseInstructions()
    {
        exitImgInstructions.gameObject.SetActive(false);
        exitTxtInstructions.gameObject.SetActive(false);
    }

    public void OpenMenu()
    {
        if (isExitOpen)
            CloseExit();

        isMenuOpen = true;

        Cursor.lockState = CursorLockMode.None;

        rifle.SetActive(true);
        shotgun.SetActive(true);
        pistol.SetActive(true);

        Time.timeScale = 0.0f;

        menuCanvas.SetActive(true);

        UpdateMenu();

        
    }

    public void CloseMenu()
    {
        isMenuOpen = false;

        Time.timeScale = 1.0f;

        if (gameObject.GetComponent<GunManager>().GetActiveWeapon() == 1)
        {
            gameObject.GetComponent<GunManager>().ActivateRifle();
        }
        else if (gameObject.GetComponent<GunManager>().GetActiveWeapon() == 2)
        {
            gameObject.GetComponent<GunManager>().ActivateShotgun();
        }
        else if (gameObject.GetComponent<GunManager>().GetActiveWeapon() == 3)
        {
            gameObject.GetComponent<GunManager>().ActivatePistol();
        }

        menuCanvas.SetActive(false);

        gameObject.GetComponent<GunManager>().SetCanChange(true);

        Cursor.lockState = CursorLockMode.Locked;
    }

    public void UpdateMenu()
    {
        rifleLevel.text = "    Level: " + rifle.GetComponent<Gun>().GetLevel() + 
            "\n    Exp: " + rifle.GetComponent<Gun>().GetCurrExp() + 
            " / " + rifle.GetComponent<Gun>().GetCurrExpBracket() + 
            "\n    Available skill points: " + rifle.GetComponent<Gun>().GetSkillPoints();

        shotgunLevel.text = "    Level: " + shotgun.GetComponent<Gun>().GetLevel() +
            "\n    Exp: " + shotgun.GetComponent<Gun>().GetCurrExp() +
            " / " + shotgun.GetComponent<Gun>().GetCurrExpBracket() +
            "\n    Available skill points: " + shotgun.GetComponent<Gun>().GetSkillPoints();

        pistolLevel.text = "    Level: " + pistol.GetComponent<Gun>().GetLevel() +
            "\n    Exp: " + pistol.GetComponent<Gun>().GetCurrExp() +
            " / " + pistol.GetComponent<Gun>().GetCurrExpBracket() +
            "\n    Available skill points: " + pistol.GetComponent<Gun>().GetSkillPoints();

        rifleSkillOneLevel.text = rifle.GetComponent<Gun>().GetSkillTree()[0].GetB() + "/10";
        rifleSkillTwoLevel.text = rifle.GetComponent<Gun>().GetSkillTree()[1].GetB() + "/10";
        rifleSkillThreeLevel.text = rifle.GetComponent<Gun>().GetSkillTree()[2].GetB() + "/10";
        rifleSkillFourLevel.text = rifle.GetComponent<Gun>().GetSkillTree()[3].GetB() + "/10";
        rifleSkillFiveLevel.text = rifle.GetComponent<Gun>().GetSkillTree()[4].GetB() + "/10";
        rifleSkillSixLevel.text = rifle.GetComponent<Gun>().GetSkillTree()[5].GetB() + "/10";
        rifleSkillSevenLevel.text = rifle.GetComponent<Gun>().GetSkillTree()[6].GetB() + "/10";
        rifleSkillEightLevel.text = rifle.GetComponent<Gun>().GetSkillTree()[7].GetB() + "/10";
        rifleSkillNineLevel.text = rifle.GetComponent<Gun>().GetSkillTree()[8].GetB() + "/10";

        shotgunSkillOneLevel.text = shotgun.GetComponent<Gun>().GetSkillTree()[0].GetB() + "/10";
        shotgunSkillTwoLevel.text = shotgun.GetComponent<Gun>().GetSkillTree()[1].GetB() + "/10";
        shotgunSkillThreeLevel.text = shotgun.GetComponent<Gun>().GetSkillTree()[2].GetB() + "/10";
        shotgunSkillFourLevel.text = shotgun.GetComponent<Gun>().GetSkillTree()[3].GetB() + "/10";
        shotgunSkillFiveLevel.text = shotgun.GetComponent<Gun>().GetSkillTree()[4].GetB() + "/10";
        shotgunSkillSixLevel.text = shotgun.GetComponent<Gun>().GetSkillTree()[5].GetB() + "/10";
        shotgunSkillSevenLevel.text = shotgun.GetComponent<Gun>().GetSkillTree()[6].GetB() + "/10";
        shotgunSkillEightLevel.text = shotgun.GetComponent<Gun>().GetSkillTree()[7].GetB() + "/10";
        shotgunSkillNineLevel.text = shotgun.GetComponent<Gun>().GetSkillTree()[8].GetB() + "/10";

        pistolSkillOneLevel.text = pistol.GetComponent<Gun>().GetSkillTree()[0].GetB() + "/10";
        pistolSkillTwoLevel.text = pistol.GetComponent<Gun>().GetSkillTree()[1].GetB() + "/10";
        pistolSkillThreeLevel.text = pistol.GetComponent<Gun>().GetSkillTree()[2].GetB() + "/10";
        pistolSkillFourLevel.text = pistol.GetComponent<Gun>().GetSkillTree()[3].GetB() + "/10";
        pistolSkillFiveLevel.text = pistol.GetComponent<Gun>().GetSkillTree()[4].GetB() + "/10";
        pistolSkillSixLevel.text = pistol.GetComponent<Gun>().GetSkillTree()[5].GetB() + "/10";
    }

    public void LevelRifleSkill(int no)
    {
        if (rifle.GetComponent<Gun>().GetSkillPoints() > 0)
        {
            if (no == 1)
            {
                if (rifle.GetComponent<Gun>().GetSkillTree()[0].GetB() < 10)
                {
                    rifle.GetComponent<Gun>().GetSkillTree()[0].SetB(rifle.GetComponent<Gun>().GetSkillTree()[0].GetB() + 1);
                    rifle.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 2)
            {
                if (rifle.GetComponent<Gun>().GetSkillTree()[1].GetB() < 10)
                {
                    rifle.GetComponent<Gun>().GetSkillTree()[1].SetB(rifle.GetComponent<Gun>().GetSkillTree()[1].GetB() + 1);
                    rifle.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 3)
            {
                if (rifle.GetComponent<Gun>().GetSkillTree()[2].GetB() < 10)
                {
                    rifle.GetComponent<Gun>().GetSkillTree()[2].SetB(rifle.GetComponent<Gun>().GetSkillTree()[2].GetB() + 1);
                    rifle.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 4)
            {
                if (rifle.GetComponent<Gun>().GetSkillTree()[3].GetB() < 10)
                {
                    rifle.GetComponent<Gun>().GetSkillTree()[3].SetB(rifle.GetComponent<Gun>().GetSkillTree()[3].GetB() + 1);
                    rifle.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 5)
            {
                if (rifle.GetComponent<Gun>().GetSkillTree()[4].GetB() < 10)
                {
                    rifle.GetComponent<Gun>().GetSkillTree()[4].SetB(rifle.GetComponent<Gun>().GetSkillTree()[4].GetB() + 1);
                    rifle.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 6)
            {
                if (rifle.GetComponent<Gun>().GetSkillTree()[5].GetB() < 10)
                {
                    rifle.GetComponent<Gun>().GetSkillTree()[5].SetB(rifle.GetComponent<Gun>().GetSkillTree()[5].GetB() + 1);
                    rifle.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 7)
            {
                if (rifle.GetComponent<Gun>().GetSkillTree()[6].GetB() < 10)
                {
                    rifle.GetComponent<Gun>().GetSkillTree()[6].SetB(rifle.GetComponent<Gun>().GetSkillTree()[6].GetB() + 1);
                    rifle.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 8)
            {
                if (rifle.GetComponent<Gun>().GetSkillTree()[7].GetB() < 10)
                {
                    rifle.GetComponent<Gun>().GetSkillTree()[7].SetB(rifle.GetComponent<Gun>().GetSkillTree()[7].GetB() + 1);
                    rifle.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 9)
            {
                if (rifle.GetComponent<Gun>().GetSkillTree()[8].GetB() < 10)
                {
                    rifle.GetComponent<Gun>().GetSkillTree()[8].SetB(rifle.GetComponent<Gun>().GetSkillTree()[8].GetB() + 1);
                    rifle.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
        }

        UpdateMenu();
    }

    public void LevelShotgunSkill(int no)
    {
        if (shotgun.GetComponent<Gun>().GetSkillPoints() > 0)
        {
            if (no == 1)
            {
                if (shotgun.GetComponent<Gun>().GetSkillTree()[0].GetB() < 10)
                {
                    shotgun.GetComponent<Gun>().GetSkillTree()[0].SetB(shotgun.GetComponent<Gun>().GetSkillTree()[0].GetB() + 1);
                    shotgun.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 2)
            {
                if (shotgun.GetComponent<Gun>().GetSkillTree()[1].GetB() < 10)
                {
                    shotgun.GetComponent<Gun>().GetSkillTree()[1].SetB(shotgun.GetComponent<Gun>().GetSkillTree()[1].GetB() + 1);
                    shotgun.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 3)
            {
                if (shotgun.GetComponent<Gun>().GetSkillTree()[2].GetB() < 10)
                {
                    shotgun.GetComponent<Gun>().GetSkillTree()[2].SetB(shotgun.GetComponent<Gun>().GetSkillTree()[2].GetB() + 1);
                    shotgun.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 4)
            {
                if (shotgun.GetComponent<Gun>().GetSkillTree()[3].GetB() < 10)
                {
                    shotgun.GetComponent<Gun>().GetSkillTree()[3].SetB(shotgun.GetComponent<Gun>().GetSkillTree()[3].GetB() + 1);
                    shotgun.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 5)
            {
                if (shotgun.GetComponent<Gun>().GetSkillTree()[4].GetB() < 10)
                {
                    shotgun.GetComponent<Gun>().GetSkillTree()[4].SetB(shotgun.GetComponent<Gun>().GetSkillTree()[4].GetB() + 1);
                    shotgun.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 6)
            {
                if (shotgun.GetComponent<Gun>().GetSkillTree()[5].GetB() < 10)
                {
                    shotgun.GetComponent<Gun>().GetSkillTree()[5].SetB(shotgun.GetComponent<Gun>().GetSkillTree()[5].GetB() + 1);
                    shotgun.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 7)
            {
                if (shotgun.GetComponent<Gun>().GetSkillTree()[6].GetB() < 10)
                {
                    shotgun.GetComponent<Gun>().GetSkillTree()[6].SetB(shotgun.GetComponent<Gun>().GetSkillTree()[6].GetB() + 1);
                    shotgun.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 8)
            {
                if (shotgun.GetComponent<Gun>().GetSkillTree()[7].GetB() < 10)
                {
                    shotgun.GetComponent<Gun>().GetSkillTree()[7].SetB(shotgun.GetComponent<Gun>().GetSkillTree()[7].GetB() + 1);
                    shotgun.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 9)
            {
                if (shotgun.GetComponent<Gun>().GetSkillTree()[8].GetB() < 10)
                {
                    shotgun.GetComponent<Gun>().GetSkillTree()[8].SetB(shotgun.GetComponent<Gun>().GetSkillTree()[8].GetB() + 1);
                    shotgun.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
        }

        UpdateMenu();
    }

    public void LevelPistolSkill(int no)
    {
        if (pistol.GetComponent<Gun>().GetSkillPoints() > 0)
        {
            if (no == 1)
            {
                if (pistol.GetComponent<Gun>().GetSkillTree()[0].GetB() < 10)
                {
                    pistol.GetComponent<Gun>().GetSkillTree()[0].SetB(pistol.GetComponent<Gun>().GetSkillTree()[0].GetB() + 1);
                    pistol.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 2)
            {
                if (pistol.GetComponent<Gun>().GetSkillTree()[1].GetB() < 10)
                {
                    pistol.GetComponent<Gun>().GetSkillTree()[1].SetB(pistol.GetComponent<Gun>().GetSkillTree()[1].GetB() + 1);
                    pistol.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 3)
            {
                if (pistol.GetComponent<Gun>().GetSkillTree()[2].GetB() < 10)
                {
                    pistol.GetComponent<Gun>().GetSkillTree()[2].SetB(pistol.GetComponent<Gun>().GetSkillTree()[2].GetB() + 1);
                    pistol.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 4)
            {
                if (pistol.GetComponent<Gun>().GetSkillTree()[3].GetB() < 10)
                {
                    pistol.GetComponent<Gun>().GetSkillTree()[3].SetB(pistol.GetComponent<Gun>().GetSkillTree()[3].GetB() + 1);
                    pistol.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 5)
            {
                if (pistol.GetComponent<Gun>().GetSkillTree()[4].GetB() < 10)
                {
                    pistol.GetComponent<Gun>().GetSkillTree()[4].SetB(pistol.GetComponent<Gun>().GetSkillTree()[4].GetB() + 1);
                    pistol.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
            else if (no == 6)
            {
                if (pistol.GetComponent<Gun>().GetSkillTree()[5].GetB() < 10)
                {
                    pistol.GetComponent<Gun>().GetSkillTree()[5].SetB(pistol.GetComponent<Gun>().GetSkillTree()[5].GetB() + 1);
                    pistol.GetComponent<Gun>().AddSkillPoints(-1);
                }
            }
        }

        UpdateMenu();
    }

    public bool GetIsMenuOpen()
    {
        return isMenuOpen;
    }

    public bool GetIsExitOpen()
    {
        return isExitOpen;
    }
}