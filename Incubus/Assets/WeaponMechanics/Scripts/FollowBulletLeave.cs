﻿using UnityEngine;
using System.Collections;

public class FollowBulletLeave: MonoBehaviour
{
    private GameObject bulletLeave;

    void Update()
    {
        transform.position = bulletLeave.transform.position;
    }

    public void SetBulletLeave(GameObject bLeave)
    {
        bulletLeave = bLeave;
    }
}