﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public float health;

    void Start()
    {
        health = 100;
    }

    public void Heal(int no)
    {
        health += no;

        if (health > 100)
        {
            health = 100;
        }
    }

    public void TakeDamage(int no)
    {
        health -= no;

        if (health < 0)
        {
            
            SceneManager.LoadScene("GameOver");
            

        }
    }

    public float getHealth()
    {
        return health;
    }
}