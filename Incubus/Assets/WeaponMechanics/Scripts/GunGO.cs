﻿using UnityEngine;
using System.Collections;

public class GunGO : MonoBehaviour
{
    public GameObject rifle;
    public GameObject shotgun;
    public GameObject pistol;

    public GameObject GetRifle()
    {
        return rifle;
    }

    public GameObject GetShotgun()
    {
        return shotgun;
    }

    public GameObject GetPistol()
    {
        return pistol;
    }
}