﻿using UnityEngine;
using System.Collections;

public class TrailForce : MonoBehaviour
{
    Transform m_transform;
    private float speed = 400.0f;
    private Vector3 endPoint = Vector3.zero;
    private bool hitSomething = false;

    void Start()
    {
        m_transform = transform;
        Destroy(gameObject, 0.5f);
    }

    void Update()
    {
        if (!hitSomething)
        {
            m_transform.position = Vector3.MoveTowards(m_transform.position, m_transform.position + m_transform.forward, Time.deltaTime * speed);
        }
        //this.GetComponent<Rigidbody>().AddForce(m_transform.forward * speed, ForceMode.Impulse);

        if (endPoint != Vector3.zero)
        {
            m_transform.position = Vector3.MoveTowards(m_transform.position, endPoint + m_transform.forward, Time.deltaTime * speed);
        }
    }

    public void SetEndPoint(Vector3 pos)
    {
        endPoint = pos;
        hitSomething = true;
    }
}