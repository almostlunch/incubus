﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScripts : MonoBehaviour {

    public GameObject DevMode;
    private bool devModeUnlocked = false;

    void Start()
    {
        DevMode.GetComponent<Button>().interactable = GameObject.Find("DevModeStuff").GetComponent<DevModeStuff>().GetDevMode();
    }

    public void PlayStart()
    {
        transform.GetComponent<AudioSource>().Play();
        Invoke("StartGame", 0);
    }


	public void StartGame()
    {
        SceneManager.LoadScene("Level1");
    }

    public void StartDev()
    {
        SceneManager.LoadScene("DevLevel");
    }

    public void PlayQuit()
    {
        transform.GetComponent<AudioSource>().Play();
        Invoke("QuitGame", 0);
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    public void SetDevMode(bool b)
    {
        devModeUnlocked = b;
    }

    public bool GetDevMode()
    {
        return devModeUnlocked;
    }
}
