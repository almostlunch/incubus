﻿using UnityEngine;
using System.Collections;

public class InteractionRay : MonoBehaviour
{
    private GameObject m_camera;
    RaycastHit[] hits;
    RaycastHit hit;
    private bool deleted = false;


    void Start()
    {
        m_camera = GameObject.FindGameObjectWithTag("MainCamera");        
    }

    void Update()
    {
        Vector3 dir = m_camera.transform.rotation * Vector3.forward * 5.0f;

        hits = Physics.RaycastAll(m_camera.transform.position, dir, 10.0f);
        foreach (RaycastHit r in hits)
        {
           
           /* if (r.collider.gameObject.tag == "MedBox" && Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("hitting");
                r.collider.transform.FindChild("MedCupboard").GetComponent<Animator>().Play("DoorOpen");
            }*/

            if (r.collider.gameObject.tag == "MedPack" && Input.GetKeyDown(KeyCode.E))
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().Heal(28);
                Destroy(r.collider.gameObject);
            }
        }

        if (Physics.Raycast(m_camera.transform.position, dir, out hit))
        {
            if (hit.collider.gameObject.tag == "Interactable")
            {
                //Get info to display to the character
            }
            if (hit.collider.gameObject.tag == "SpawnHatch" && Input.GetKeyDown(KeyCode.E))
            {
                if (deleted == false)
                {
                    deleted = true;
                    GameObject.FindGameObjectWithTag("WorldText").SetActive(false);
                    GameObject.FindGameObjectWithTag("LoadingText").SetActive(false);
                    GameObject[] gos = GameObject.FindGameObjectsWithTag("Pressure");
                    foreach (GameObject go in gos)
                    {
                        go.GetComponent<ParticleSystem>().Play();
                    }
                    transform.GetComponent<AudioSource>().Play();
                    StartCoroutine(Delay());
                    deleted = false;
                }
            }
            /*
            if(hit.collider.gameObject.tag == "MedBox" && Input.GetKeyDown(KeyCode.E))
            {
                hit.collider.transform.FindChild("MedCupboard").GetComponent<Animator>().Play("DoorOpen");
            }

            if (hit.collider.gameObject.tag == "MedPack" && Input.GetKeyDown(KeyCode.E))
            {

            }*/

            
        }
    }

    IEnumerator Delay() {
        yield return new WaitForSeconds(2.4f);
        GameObject.FindGameObjectWithTag("SpawnHatch").GetComponent<Rigidbody>().isKinematic = false;
        Invoke("ChangeTag", 3);
   
    }

    void ChangeTag()
    {
        GameObject.FindGameObjectWithTag("SpawnHatch").tag = "Wall";
    }

}