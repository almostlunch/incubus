﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevModeStuff : MonoBehaviour
{
    private SaveLoad saveLoad;
    private bool devMode;
    private bool playDevMode = false;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        saveLoad = new SaveLoad();
        devMode = false;

        LoadGame();
    }

    public void SetDevMode(bool b)
    {
        devMode = b;
        SaveGame();
    }

    public bool GetDevMode()
    {
        return devMode;
    }

    public void SaveGame()
    {
        saveLoad.SaveXml();
    }

    public void LoadGame()
    {
        saveLoad.LoadXml();
    }

    public void setPlayDev()
    {
        playDevMode = true;
    }

    public bool GetPlayDev()
    {
        return playDevMode;
    }
}