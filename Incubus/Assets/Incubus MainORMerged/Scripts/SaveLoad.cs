﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class SaveLoad
{
    public void SaveXml()
    {
        SaveFile saveFile = new SaveFile();

        saveFile.devMode = GameObject.Find("DevModeStuff").GetComponent<DevModeStuff>().GetDevMode();

        XmlSerializer serializer = new XmlSerializer(typeof(SaveFile));
        FileStream stream = new FileStream(Application.dataPath + "/Saves/SaveData.xml", FileMode.Create);
        serializer.Serialize(stream, saveFile);
        stream.Close();
    }

    public void LoadXml()
    {
        SaveFile saveFile = new SaveFile();

        XmlSerializer serializer = new XmlSerializer(typeof(SaveFile));
        FileStream stream = new FileStream(Application.dataPath + "/Saves/SaveData.xml", FileMode.Open);
        saveFile = serializer.Deserialize(stream) as SaveFile;
        stream.Close();

        GameObject.Find("DevModeStuff").GetComponent<DevModeStuff>().SetDevMode(saveFile.devMode);
    }
}