﻿using UnityEngine;
using System.Collections;

public class CameraManagement : MonoBehaviour {

void Awake()
    {
        
        GameObject.FindGameObjectWithTag("Player").transform.FindChild("TheCamera").FindChild("Main Camera").GetComponent<Camera>().rect = new Rect(0, 0, 0, 0);
        GameObject.FindGameObjectWithTag("Player").transform.FindChild("TheCamera").FindChild("Main Camera").FindChild("OtherCam").GetComponent<Camera>().rect = new Rect(0, 0, 0, 0);

        GameObject.FindGameObjectWithTag("LoadingText").SetActive(true);
    }
}
