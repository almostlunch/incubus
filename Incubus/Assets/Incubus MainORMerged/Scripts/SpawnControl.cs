﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpawnControl : MonoBehaviour {

    bool hasSpawned = false;
    bool spawn = true;
    public GameObject loadText;
    private Transform childLoadText;
    private int medBay;
    private int maintenance;
    private int storage;
    private int demonNest;
    private float textTimer = 0.05f;
    private int pointer = 0;
    private int medC = 0;

    private string loadStr;
    //private char[] finishedLoadStr;

    private GameObject mM, canvasM;
    void Start()
    {
        Debug.Log(transform.name);
        mM = GameObject.FindGameObjectWithTag("MiniMap");
        canvasM = GameObject.FindGameObjectWithTag("CanvasMain");
        mM.SetActive(false);
        canvasM.SetActive(false);
        loadStr = "Scanning...\nLocal space station found...\nPreparing to board...\nScanning for lifeforms...\nWARNING: Demon lifeform detected..\nTHREAT LEVEL: DEADLY\nDESIGNATION: Incubus\nScanning rooms...";
        //finishedLoadStr = new char[loadStr.Length];
        loadText = GameObject.FindGameObjectWithTag("LoadCanvas");

        for (int i = 0; i < loadText.transform.childCount; i++)
        {
            if (loadText.transform.GetChild(i).tag == "LoadingText")
                childLoadText = loadText.transform.GetChild(i);
        }
    }

    void Update()
    {
        if (GameObject.FindGameObjectWithTag("GameWorldManager").GetComponent<GameWorld>().canSpawn == true && spawn && hasSpawned)
        {
            spawn = false;
            mM.SetActive(true);
            canvasM.SetActive(true);
            GameObject.FindGameObjectWithTag("Player").transform.FindChild("TheCamera").FindChild("Main Camera").GetComponent<Camera>().rect = new Rect(0, 0, 1, 1);
            GameObject.FindGameObjectWithTag("Player").transform.FindChild("TheCamera").FindChild("Main Camera").FindChild("OtherCam").GetComponent<Camera>().rect = new Rect(0, 0, 1, 1);
            //GameObject.FindGameObjectWithTag("Canvas").SetActive(true);
            Destroy(GameObject.FindGameObjectWithTag("LoadCam"));

            int medC = GameObject.FindGameObjectWithTag("GameWorldManager").GetComponent<GameWorld>().medCount;
            
            medBay = GameObject.FindGameObjectWithTag("GameWorldManager").GetComponent<GameWorld>().mbC /3;
            maintenance = GameObject.FindGameObjectWithTag("GameWorldManager").GetComponent<GameWorld>().mC /3;
            storage = GameObject.FindGameObjectWithTag("GameWorldManager").GetComponent<GameWorld>().sC /3;
            demonNest = GameObject.FindGameObjectWithTag("GameWorldManager").GetComponent<GameWorld>().dnC /3;

            loadStr += "\nFOUND:\nMedbay No: " + medBay + "\nMaintenance No: " + maintenance + "\nStorage No: " + storage + "\nDemon Nest No: " + demonNest + "\nScanning for useful equipment...\nFOUND: \nMedical Supply Caches: " + medC; 
        }

        textTimer -= Time.deltaTime;

        if (textTimer <= 0.0f)
        {
            //finishedLoadStr[pointer] = loadStr.ToCharArray()[pointer];

            if (loadStr.ToCharArray().Length > pointer)
            {
                childLoadText.GetComponent<Text>().text += loadStr.ToCharArray()[pointer];
                pointer++;
                textTimer = 0.05f;
            }
        }
    }

void PlayerSpawn()
    {
        transform.GetComponent<Animator>().enabled = false;
        transform.FindChild("jet PS").gameObject.SetActive(false);
        hasSpawned = true;
    }
}
