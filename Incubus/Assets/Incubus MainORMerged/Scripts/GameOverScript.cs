﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour {

	public void Restart()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
