﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomFinder : MonoBehaviour {

    private UnityEngine.AI.NavMeshAgent roomFindingCube;
    public List<GameObject> Roomtiles;
    private UnityEngine.AI.NavMeshPath path;

    private bool Searching = false;

    
	// Use this for initialization
	void Start () {

        path = new UnityEngine.AI.NavMeshPath();
        roomFindingCube = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();

    }
	
	// Update is called once per frame
	void Update () {
        if(Searching)
        {
            foreach ( GameObject tile in GameObject.FindGameObjectsWithTag("Tile"))
            {
                roomFindingCube.CalculatePath(tile.transform.parent.position, path);
               if (path.status == UnityEngine.AI.NavMeshPathStatus.PathComplete)
                {
                    Roomtiles.Add(tile);
                    tile.transform.parent.name = transform.name;
                   // Debug.Log("searching" + tile.transform.position);
                }
            }

            Searching = false;
        }
	
	}


    public void SearchRoom()
    {
        Searching = true;
    }
}
