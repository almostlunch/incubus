﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

[XmlRoot("SaveFile")]
public class SaveFile
{
    [XmlAttribute("DevMode")]
    public bool devMode;

    public bool GetDevMode()
    {
        return devMode;
    }
}