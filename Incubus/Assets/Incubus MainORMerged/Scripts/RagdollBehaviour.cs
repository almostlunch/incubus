﻿using UnityEngine;
using System.Collections;

public class RagdollBehaviour : MonoBehaviour {

    float counter;
    public Rigidbody pelvis;
	void Start () {

        counter = 0;
	}
	
	// Update is called once per frame
	void Update () {

        counter += Time.deltaTime;

        if(counter > 20)
        {
            Destroy(transform.parent.gameObject);
        }
	
	}
}
