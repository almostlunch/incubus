﻿using UnityEngine;
using System.Collections;

public class OpenDoor : MonoBehaviour {

    public Animator doorCTRL;

    public bool doorOpened = false;
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && !doorOpened && GameObject.FindGameObjectsWithTag("Damageable").Length == 0 && GameObject.FindGameObjectsWithTag("SpawnPortal").Length == 0)
        {
            doorOpened = true;
            doorCTRL.Play("DoorRise");
        }
    }

    /*void Update()
    {
        if (doorOpened && doorClosed)
        {
            Debug.Log("Here");
            GameObject.FindGameObjectWithTag("ScriptManager").GetComponent<RoomFinder>().SearchRoom();
        }

        if(GameObject.FindGameObjectsWithTag("Damageable") == null)
        {
            Debug.Log("Here");
            doorOpened = false;
        }
    }*/




}
