﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour {


    public Transform[] destroy;
    private bool destroyed = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if(SceneManager.GetActiveScene().Equals(SceneManager.GetSceneByName("GameOver")) && destroyed == false)
        {
            foreach(Transform t in destroy)
            {
                Destroy(t.gameObject);
            }
            destroyed = true;
        }
	
	}
}
