﻿using UnityEngine;
using System.Collections;

public class Destoryable : MonoBehaviour
{
    public GameObject brokenBarrel;
    public GameObject brokenPipe;

    private int health = 10;

	public void Wreck()
    {
        if (gameObject.tag == "Barrel")
        {
            health -= 3;

            if (health < 0)
            {
                Instantiate(brokenBarrel, gameObject.transform.position, gameObject.transform.rotation);
                Destroy(gameObject);
            }
        }
        else if (gameObject.tag == "Pepe")
        {
            health -= 3;

            if (health < 0)
            {
                Instantiate(brokenPipe, gameObject.transform.position, gameObject.transform.rotation);
                Destroy(gameObject);
            }
        }
    }
}
